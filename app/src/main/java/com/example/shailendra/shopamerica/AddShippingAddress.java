package com.example.shailendra.shopamerica;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.country_list.CountryResponse;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.Result;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.ShippingAddressChangeRequest;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.ShippingAddressResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddShippingAddress extends BaseActivity {

    String[] country_array;
    String[] country_id_array;
    ArrayAdapter<String> country_adapter;

    @Bind(R.id.etRecipentName)
    EditText etRecipentName;

    @Bind(R.id.etaddressLine1)
    EditText etaddressLine1;

    @Bind(R.id.etAddressLine2)
    EditText etAddressLine2;

    @Bind(R.id.etCity)
    EditText etCity;

    @Bind(R.id.etPinCode)
    EditText etPinCode;

    @Bind(R.id.etState)
    EditText etState;

    @Bind(R.id.addButton)
    Button addButton;

    @Bind(R.id.spinnerCountry)
    fr.ganfra.materialspinner.MaterialSpinner spinnerCountry;

    @Bind(R.id.cbDefaultAddress)
    CheckBox cbDefaultAddress;

    @Bind(R.id.input_layout_recipentname)
    TextInputLayout input_layout_recipentname;

    @Bind(R.id.input_layout_addressLine1)
    TextInputLayout input_layout_addressLine1;


    @Bind(R.id.input_layout_city)
    TextInputLayout input_layout_city;

    @Bind(R.id.input_layout_state)
    TextInputLayout input_layout_state;

    @Bind(R.id.input_layout_pincode)
    TextInputLayout input_layout_pincode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shipping_address);
        ButterKnife.bind(this);

         /*Action Bar title and back button set*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.actionbar, null), new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.tcActivityTitle);
        titleTxtView.setText("Add Shipping Address");
        getCountryList();
        /*************************End**************************************************************************/


        etRecipentName.addTextChangedListener(new MyTextWatcher(etRecipentName));
        etaddressLine1.addTextChangedListener(new MyTextWatcher(etaddressLine1));
        etCity.addTextChangedListener(new MyTextWatcher(etCity));
        etState.addTextChangedListener(new MyTextWatcher(etState));
        etPinCode.addTextChangedListener(new MyTextWatcher(etPinCode));

        Intent i=getIntent();
        if(i.hasExtra("type"))
        {
            titleTxtView.setText("Edit Shipping Address");
            Bundle args = i.getBundleExtra("BUNDLE");
            ArrayList<Result> object = (ArrayList<Result>) args.getSerializable("ARRAYLIST");
            etRecipentName.setText(object.get(0).getReceiptName());
            etaddressLine1.setText(object.get(0).getAddress());
            etCity.setText(object.get(0).getCity());
            etState.setText(object.get(0).getState());
            etPinCode.setText(object.get(0).getPinCode());


        }


    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick(R.id.addButton)
    public void addAddress()
    {
        if (!validateRecipentName()) {
            return;
        }

        if (!validateAddress1()) {
            return;
        }

        if (!validateCity()) {
            return;
        }

        if (!validatePincode()) {
            return;
        }

        if (!validateState()) {
            return;
        }

        addShippingAddress();
    }

    private boolean validateRecipentName() {
        if (etRecipentName.getText().toString().trim().isEmpty()) {
            input_layout_recipentname.setError(getString(R.string.enter_recipent_name));
            requestFocus(etRecipentName);
            return false;
        } else {
            input_layout_recipentname.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateAddress1() {
        if (etaddressLine1.getText().toString().trim().isEmpty()) {
            input_layout_addressLine1.setError(getString(R.string.enter_addrees));
            requestFocus(etaddressLine1);
            return false;
        } else {
            input_layout_addressLine1.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty()) {
            input_layout_city.setError(getString(R.string.enter_city));
            requestFocus(etCity);
            return false;
        } else {
            input_layout_city.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePincode() {
        if (etPinCode.getText().toString().trim().isEmpty()) {
            input_layout_pincode.setError(getString(R.string.enter_pincode));
            requestFocus(etPinCode);
            return false;
        } else {
            input_layout_pincode.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateState() {
        if (etState.getText().toString().trim().isEmpty()) {
            input_layout_state.setError(getString(R.string.enter_state));
            requestFocus(etState);
            return false;
        } else {
            input_layout_state.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private void getCountryList() {

        if (Utils.isNetworkAvailable(this)) {
            showLoader(AddShippingAddress.this, "Fetching Countries...");
            FSApplication.getRestClient().getCountryList(new StringRequestCallback<CountryResponse>() {
                @Override
                public void onRestResponse(Exception e, CountryResponse result) {
                    if (e == null && result != null) {
                        if (result.getStatus().equals("true")) {
                            country_array = new String[result.getResult().length];
                            country_id_array = new String[result.getResult().length];


                            for (int i = 0; i < result.getResult().length; i++) {
                                country_array[i] = result.getResult()[i].getCountry_name();
                                country_id_array[i] = result.getResult()[i].getCountry_id();
                            }
                            country_adapter = new ArrayAdapter<String>(AddShippingAddress.this, android.R.layout.simple_spinner_item, country_array);
                            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerCountry.setAdapter(country_adapter);

                        } else {
                            Toast.makeText(AddShippingAddress.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                    hideLoader();

                }

            });
        } else {
            Toast.makeText(AddShippingAddress.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }


    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etRecipentName:
                    validateRecipentName();
                    break;
                case R.id.etaddressLine1:
                    validateAddress1();
                    break;
                case R.id.etCity:
                    validateCity();
                    break;
                case R.id.etPinCode:
                    validatePincode();
                    break;
                case R.id.etState:
                    validateState();
                    break;


            }
        }
    }


    private void addShippingAddress()
    {
        if (Utils.isNetworkAvailable(AddShippingAddress.this)) {
            showLoader(AddShippingAddress.this,"Please wait...");

            ShippingAddressChangeRequest request=new ShippingAddressChangeRequest();
            request.setUserid(AppPrefrence.getInstance().getUserId(AddShippingAddress.this));
            request.setReceiptName(etRecipentName.getText().toString().trim());
            request.setAddress(etaddressLine1.getText().toString().trim()+" "+etAddressLine2.getText().toString().trim());
            request.setCity(etCity.getText().toString().trim());
            request.setState(etState.getText().toString().trim());
            request.setPinCode(etPinCode.getText().toString().trim());
            request.setCountry(spinnerCountry.getSelectedItem().toString());
            if(cbDefaultAddress.isChecked())
            request.setDefault("1");
            else
            request.setDefault("0");

            FSApplication.getRestClient().addShippingAddress(new StringRequestCallback<ShippingAddressResponse>() {
                @Override
                public void onRestResponse(Exception e, ShippingAddressResponse result) {
                    if (e == null && result != null) {

                        Toast.makeText(AddShippingAddress.this,result.getMessage(),Toast.LENGTH_SHORT).show();


                    }
                    hideLoader();

                }

            }, request);
        } else {
            Toast.makeText(AddShippingAddress.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }

}
