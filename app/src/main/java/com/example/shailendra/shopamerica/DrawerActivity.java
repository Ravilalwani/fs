package com.example.shailendra.shopamerica;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.dummy.DummyContent;
import com.example.shailendra.shopamerica.fragments.AddressFragment;
import com.example.shailendra.shopamerica.fragments.ChangePassFragment;
import com.example.shailendra.shopamerica.fragments.ChangePasswordFragment;
import com.example.shailendra.shopamerica.fragments.FragmentConverters;
import com.example.shailendra.shopamerica.fragments.ItemFragment2;
import com.example.shailendra.shopamerica.fragments.NewAddressFragment;
import com.example.shailendra.shopamerica.fragments.PlansFragment;
import com.example.shailendra.shopamerica.fragments.ProfileFragment;
import com.example.shailendra.shopamerica.fragments.ShippingRatesFragment;
import com.example.shailendra.shopamerica.fragments.TestimonailFragment;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.squareup.picasso.Picasso;

//
public class DrawerActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ItemFragment2.OnInboxListFragmentInteractionListener, ChangePassFragment.OnFragmentInteractionListener, NewAddressFragment.OnFragmentInteractionListener {


    private TextView mTextViewName;
    private ImageButton mButtonEditProfile;
    private de.hdodenhof.circleimageview.CircleImageView profileImage;



    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorBlue));
        }
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mButtonEditProfile=(ImageButton)toolbar.findViewById(R.id.btnProfileEdit);




        mButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DrawerActivity.this,EditProfileActivity.class));
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
        View view=navigationView.getHeaderView(0);
        mTextViewName=(TextView)view.findViewById(R.id.tvName);
        profileImage=(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.imageViewProfile);



        navigationView.setItemIconTintList(null);
        Fragment fragment = new ProfileFragment();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.actionbar, null), new ActionBar.LayoutParams(
                  ActionBar.LayoutParams.WRAP_CONTENT,
                  ActionBar.LayoutParams.MATCH_PARENT,
                  Gravity.CENTER
          ));
        //getSupportActionBar().setCustomView(R.layout.actionbar);

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.addToBackStack("");
            ft.commit();

        }

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.address);
        toolbar.setOverflowIcon(drawable);

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mTextViewName.setText(AppPrefrence.getInstance().getUserName(DrawerActivity.this));


                Picasso.with(DrawerActivity.this).load(AppPrefrence.getInstance().getUSerImageUrl(DrawerActivity.this)).placeholder(R.drawable.profile_pic_main).error(R.drawable.profile_pic_main).into(profileImage);

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });



    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.mainFrame);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(f instanceof ProfileFragment)
        {
            finish();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_profile) {
            fragment = new ProfileFragment();
        } else if (id == R.id.nav_plan) {
            fragment = new PlansFragment();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_wallet) {


        } else if (id == R.id.nav_trans) {


        } else if (id == R.id.nav_shop) {
            fragment = new AddressFragment();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_request) {



        } else if (id == R.id.nav_pass) {
            fragment = new ChangePasswordFragment();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_inbound) {
            fragment = new ItemFragment2();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        }else if (id == R.id.nav_converter) {
            fragment=new FragmentConverters();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        }else if (id == R.id.nav_testimonial) {
            fragment=new TestimonailFragment();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        }
        else if (id == R.id.nav_shipping_rates) {

            fragment=new ShippingRatesFragment();
            mButtonEditProfile.setVisibility(View.INVISIBLE);
        }
        else if (id == R.id.nav_Logout) {
            showLoader(DrawerActivity.this,"Logging out....");
            AppPrefrence.getInstance().clearPrefrences(DrawerActivity.this);
            hideLoader();

            Intent i=new Intent(DrawerActivity.this,GetInActivity.class);
            startActivity(i);
            finish();

        }

        if (fragment != null) {
            getSupportActionBar().setTitle(item.getTitle());
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.addToBackStack("");
            ft.commit();
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void setActionBarTitle(String title){

        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.tcActivityTitle);
        titleTxtView.setText(title);

    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }


}
