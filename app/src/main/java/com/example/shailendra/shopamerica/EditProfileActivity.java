package com.example.shailendra.shopamerica;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.country_list.CountryResponse;
import com.example.shailendra.shopamerica.rest_api.model.edit_profile.EditProfileRequest;
import com.example.shailendra.shopamerica.rest_api.model.edit_profile.EditProfileResponse;
import com.example.shailendra.shopamerica.rest_api.model.user_profile.ProfileResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileActivity extends BaseActivity {

    String[] country_array;
    String[] country_id_array;
    ArrayAdapter<String> country_adapter;

    @Bind(R.id.etFirstName)
    EditText etFirstName;

    @Bind(R.id.etMiddleName)
    EditText etMiddleName;

    @Bind(R.id.etLastName)
    EditText etLastName;

    @Bind(R.id.etDateOfBirth)
    EditText etDateOfBirth;

    @Bind(R.id.etCompany)
    EditText etCompany;

    @Bind(R.id.etTaxId)
    EditText etTaxId;

    @Bind(R.id.etUserName)
    EditText etUserName;

    @Bind(R.id.etAddress1)
    EditText etAddress1;

    @Bind(R.id.etAddress2)
    EditText etAddress2;

    @Bind(R.id.etCity)
    EditText etCity;

    @Bind(R.id.etPinCode)
    EditText etPinCode;

    @Bind(R.id.etState)
    EditText etState;

    @Bind(R.id.etPhone1)
    EditText etPhone1;

    @Bind(R.id.etPhone2)
    EditText etPhone2;

    @Bind(R.id.etMobileNumber)
    EditText etMobileNumber;

    @Bind(R.id.etFaxNumber)
    EditText etFaxNumber;

    @Bind(R.id.etEmailId)
    EditText etEmailId;

    @Bind(R.id.updateButton)
    Button updateButton;


    @Bind(R.id.input_layout_firstname)
    TextInputLayout input_layout_firstname;

    @Bind(R.id.input_layout_lasnamename)
    TextInputLayout input_layout_lasnamename;

    @Bind(R.id.input_layout_address1)
    TextInputLayout input_layout_address1;

    @Bind(R.id.input_layout_city)
    TextInputLayout input_layout_city;

    @Bind(R.id.input_layout_state)
    TextInputLayout input_layout_state;

    @Bind(R.id.input_layout_pincode)
    TextInputLayout input_layout_pincode;

    @Bind(R.id.input_layout_emailid)
    TextInputLayout input_layout_emailid;

    @Bind(R.id.input_layout_mobile)
    TextInputLayout input_layout_mobile;

    @Bind(R.id.radiobtnMale)
    RadioButton radiobtnMale;

    @Bind(R.id.radiobtnFemale)
    RadioButton radiobtnFemale;

    @Bind(R.id.radiobtnIndividual)
    RadioButton radiobtnIndividual;

    @Bind(R.id.radiobtnBusiness)
    RadioButton radiobtnBusiness;

    @Bind(R.id.spinnerCountry)
    fr.ganfra.materialspinner.MaterialSpinner spinnerCountry;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        /*Action Bar title and back button set*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.actionbar, null), new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.tcActivityTitle);
        titleTxtView.setText("Edit Profile");

        /*************************End**************************************************************************/


        etFirstName.addTextChangedListener(new MyTextWatcher(etFirstName));
        etLastName.addTextChangedListener(new MyTextWatcher(etLastName));
        etAddress1.addTextChangedListener(new MyTextWatcher(etAddress1));
        etCity.addTextChangedListener(new MyTextWatcher(etCity));
        etState.addTextChangedListener(new MyTextWatcher(etState));
        etPinCode.addTextChangedListener(new MyTextWatcher(etPinCode));
        etMobileNumber.addTextChangedListener(new MyTextWatcher(etMobileNumber));
        etEmailId.addTextChangedListener(new MyTextWatcher(etEmailId));

        getProfile();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick(R.id.updateButton)
    public void updateProfile()
    {
        submitForm();
    }


    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateFirstName()) {
            return;
        }

        if (!validateLastName()) {
            return;
        }

        if (!validateAddress1()) {
            return;
        }

        if (!validateCity()) {
            return;
        }

        if (!validatePincode()) {
            return;
        }

        if (!validateState()) {
            return;
        }

        if (!validateMobile()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }



        doEditProfile();
    }

    private boolean validateFirstName() {
        if (etFirstName.getText().toString().trim().isEmpty()) {
            input_layout_firstname.setError(getString(R.string.enter_first_name));
            requestFocus(etFirstName);
            return false;
        } else {
            input_layout_firstname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName() {
        if (etLastName.getText().toString().trim().isEmpty()) {
            input_layout_lasnamename.setError(getString(R.string.enter_last_name));
            requestFocus(etLastName);
            return false;
        } else {
            input_layout_lasnamename.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress1() {
        if (etAddress1.getText().toString().trim().isEmpty()) {
            input_layout_address1.setError(getString(R.string.enter_addrees));
            requestFocus(etAddress1);
            return false;
        } else {
            input_layout_address1.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty()) {
            input_layout_city.setError(getString(R.string.enter_city));
            requestFocus(etCity);
            return false;
        } else {
            input_layout_city.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePincode() {
        if (etPinCode.getText().toString().trim().isEmpty()) {
            input_layout_pincode.setError(getString(R.string.enter_pincode));
            requestFocus(etPinCode);
            return false;
        } else {
            input_layout_pincode.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateState() {
        if (etState.getText().toString().trim().isEmpty()) {
            input_layout_state.setError(getString(R.string.enter_state));
            requestFocus(etState);
            return false;
        } else {
            input_layout_state.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etMobileNumber.getText().toString().trim().isEmpty()) {
            input_layout_mobile.setError(getString(R.string.enter_mobile));
            requestFocus(etMobileNumber);
            return false;
        } else {
            input_layout_mobile.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (etEmailId.getText().toString().trim().isEmpty()) {
            input_layout_emailid.setError(getString(R.string.enter_email));
            requestFocus(etEmailId);
            return false;
        } else  if (isValidEmail(etEmailId.getText().toString().trim())) {
            input_layout_emailid.setError(getString(R.string.enter_valid_email));
            requestFocus(etEmailId);
            return false;
        }else{
            input_layout_emailid.setErrorEnabled(false);
        }

        return true;
    }



    private static boolean isValidEmail(String email) {
        return !Utils.validateEmailPattern(email);
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etFirstName:
                    validateFirstName();
                    break;
                case R.id.etLastName:
                    validateLastName();
                    break;
                case R.id.etAddress1:
                    validateAddress1();
                    break;
                case R.id.etCity:
                    validateCity();
                    break;
                case R.id.etPinCode:
                    validatePincode();
                    break;
                case R.id.etState:
                    validateState();
                    break;
                case R.id.etMobileNumber:
                    validateMobile();
                    break;
                case R.id.etEmailId:
                    validateEmail();
                    break;

            }
        }
    }

    private void getProfile() {
        if (Utils.isNetworkAvailable(this)) {
            showLoader(EditProfileActivity.this,"Please wait...");
            String userid= AppPrefrence.getInstance().getUserId(EditProfileActivity.this);

            FSApplication.getRestClient().getUserProfile(new StringRequestCallback<ProfileResponse>() {
                @Override
                public void onRestResponse(Exception e, ProfileResponse result) {
                    if (e == null && result != null) {

                       /* AppPrefrence.getInstance().saveImageUrl(getActivity(),result.getResult().getProfileImg());
                        AppPrefrence.getInstance().saveUserName(getActivity(),result.getResult().getFirst_Name());*/

                        //Glide.with(getActivity()).load(result.getResult().getProfileImg()).placeholder(R.drawable.profile_pic_main).error(R.drawable.profile_pic_main).into(mImageViewProfile);
                        etFirstName.setText(result.getResult().getFirst_Name());
                        etMiddleName.setText(result.getResult().getMiddleName());
                        etLastName.setText(result.getResult().getLast_Name());
                        if(result.getResult().getGender().equalsIgnoreCase("female"))
                        {
                            radiobtnFemale.setChecked(true);
                        }
                        else
                        {
                            radiobtnMale.setChecked(true);
                        }

                        etDateOfBirth.setText(result.getResult().getDob());
                        etCompany.setText(result.getResult().getCompany());
                        etTaxId.setText(result.getResult().getTaxID());
                        etUserName.setText(result.getResult().getEmail());
                        etUserName.setEnabled(false);
                        if(result.getResult().getUserType().equalsIgnoreCase("Business"))
                        {
                            radiobtnBusiness.setChecked(true);
                        }
                        else
                        {
                            radiobtnIndividual.setChecked(true);
                        }
                        etAddress1.setText(result.getResult().getAddress());
                        etMobileNumber.setText(result.getResult().getPhone1());
                        etPhone1.setText(result.getResult().getPhone2());
                        etEmailId.setText(result.getResult().getEmail());
                        etFaxNumber.setText(result.getResult().getFaxNo());
                        hideLoader();
                        getCountryList("");
                    }
                    hideLoader();

                }

            },userid);
        } else {
            Toast.makeText(EditProfileActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }

    }

    private void getCountryList(final String country) {

        if (Utils.isNetworkAvailable(this)) {
            showLoader(EditProfileActivity.this, "Fetching Countries...");
            FSApplication.getRestClient().getCountryList(new StringRequestCallback<CountryResponse>() {
                @Override
                public void onRestResponse(Exception e, CountryResponse result) {
                    if (e == null && result != null) {
                        if (result.getStatus().equals("true")) {
                            country_array = new String[result.getResult().length];
                            country_id_array = new String[result.getResult().length];


                            for (int i = 0; i < result.getResult().length; i++) {
                                country_array[i] = result.getResult()[i].getCountry_name();
                                country_id_array[i] = result.getResult()[i].getCountry_id();
                            }
                            country_adapter = new ArrayAdapter<String>(EditProfileActivity.this, android.R.layout.simple_spinner_item, country_array);
                            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerCountry.setAdapter(country_adapter);

                            if(Arrays.asList(country_array).contains(country))
                            {
                                spinnerCountry.setSelection(Arrays.asList(country_array).indexOf(country)+1);
                            }

                        } else {
                            Toast.makeText(EditProfileActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                    hideLoader();

                }

            });
        } else {
            Toast.makeText(EditProfileActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }


    }

    private void doEditProfile()
    {
        if (Utils.isNetworkAvailable(this)) {
            showLoader(EditProfileActivity.this, "Please Wait...");

            EditProfileRequest request=new EditProfileRequest();
            request.setUserid(AppPrefrence.getInstance().getUserId(EditProfileActivity.this));
            request.setFirstname(etFirstName.getText().toString().trim());
            request.setMiddlename(etMiddleName.getText().toString().trim());
            request.setLastname(etLastName.getText().toString().trim());
            request.setDob(etDateOfBirth.getText().toString().trim());
            if(radiobtnMale.isChecked())
                request.setGender(radiobtnMale.getText().toString());
            else
                request.setGender(radiobtnFemale.getText().toString());
            request.setEmail(etEmailId.getText().toString().trim());
            request.setAddress1(etAddress1.getText().toString().trim());
            request.setAddress2(etAddress2.getText().toString().trim());
            request.setCity(etCity.getText().toString().trim());
            request.setPincode(etPinCode.getText().toString().trim());
            request.setState(etState.getText().toString().trim());
            request.setCountry(spinnerCountry.getSelectedItem().toString());
            request.setTaxid(etTaxId.getText().toString().trim());
            request.setCountrycode1("");
            request.setLocalcode1("");
            request.setPhno1(etPhone1.getText().toString().trim());
            request.setPhno2(etPhone2.getText().toString().trim());
            request.setMobile_no(etMobileNumber.getText().toString().trim());
            request.setFaxno(etFaxNumber.getText().toString().trim());
            request.setUsername(etUserName.getText().toString().trim());
            request.setCompany(etCompany.getText().toString().trim());
            request.setCountrycode2("");
            request.setCountrycode3("");
            request.setLocalcode2("");



            FSApplication.getRestClient().doEditProfile(new StringRequestCallback<EditProfileResponse>() {
                @Override
                public void onRestResponse(Exception e, EditProfileResponse result) {
                    if (e == null && result != null) {
                        Toast.makeText(EditProfileActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();

                    }
                    hideLoader();

                }

            },request);
        } else {
            Toast.makeText(EditProfileActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }

}
