package com.example.shailendra.shopamerica;

import android.app.Application;

import com.example.shailendra.shopamerica.rest_api.RestClient;

/**
 * Created by daffodil on 3/6/16.
 */
public class FSApplication extends Application {

    public static RestClient sRestClient;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }


    private void init() {
        sRestClient = new RestClient(this);

    }

    public static RestClient getRestClient() {
        return sRestClient;
    }

}
