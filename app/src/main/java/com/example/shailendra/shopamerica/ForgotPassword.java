package com.example.shailendra.shopamerica;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.forgot_password.ForgotPasswordResponse;
import com.example.shailendra.shopamerica.utils.Utils;

public class ForgotPassword extends BaseActivity {
    private CoordinatorLayout main;
    private  LinearLayout form;
    private Button submit;
    EditText mEditTextForgotEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


          /*Action Bar title and back button set*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.actionbar, null), new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.tcActivityTitle);
        titleTxtView.setText("Forgot Password");

        /*************************End**************************************************************************/



          /* adapt the image to the size of the display */
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                getResources(),R.drawable.background),size.x,size.y,true);
//        back = (ImageView) findViewById(R.id.back);
//        back.setImageBitmap(bmp);

        main = (CoordinatorLayout) findViewById(R.id.main);
        mEditTextForgotEmail=(EditText)findViewById(R.id.forgot_email);
        Drawable d = new BitmapDrawable(getResources(), bmp);
        main.setBackground(d);

        form = (LinearLayout) findViewById(R.id.email_login_form);
        Drawable  dcontent = getResources().getDrawable(R.drawable.content_bg);
        dcontent.setAlpha(128);
        form.setBackground(dcontent);

        submit = (Button) findViewById(R.id.forgot_button) ;
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword(mEditTextForgotEmail.getText().toString().trim());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private boolean validateFields()
    {
        if ((TextUtils.isEmpty(mEditTextForgotEmail.getText().toString().trim()))) {
            mEditTextForgotEmail.setError(getString(R.string.signup_email_blank));
            mEditTextForgotEmail.requestFocus();
            return false;
        }
        if (!Utils.validateEmailPattern(mEditTextForgotEmail.getText().toString().trim())) {
            mEditTextForgotEmail.setError(getString(R.string.signup_email_invalid));
            mEditTextForgotEmail.requestFocus();
            return false;
        }
        return  true;
    }

    private void forgotPassword(String email) {
        if (validateFields()) {
            if (Utils.isNetworkAvailable(this)) {
                showLoader(ForgotPassword.this,"Please wait...");

                FSApplication.getRestClient().forgotPassword(new StringRequestCallback<ForgotPasswordResponse>() {
                    @Override
                    public void onRestResponse(Exception e, ForgotPasswordResponse result) {
                        if (e == null && result != null) {

                            Toast.makeText(ForgotPassword.this,result.getMessage(),Toast.LENGTH_SHORT).show();

                        }
                        hideLoader();

                    }

                }, email);
            } else {
                Toast.makeText(ForgotPassword.this, "No Internet Available", Toast.LENGTH_LONG).show();

            }
        }

    }
}
