package com.example.shailendra.shopamerica;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.login.LoginRequest;
import com.example.shailendra.shopamerica.rest_api.model.login.LoginResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetInActivity extends BaseActivity {
    private RelativeLayout mains;
    private TextView signup,forgot;
    private Button signin;
    private LinearLayout signup_layout;

    @Bind(R.id.editTextEmail)
    EditText mEditTextEmail;

    @Bind(R.id.editTextPassword)
    EditText mEditTextPassword;

    @Bind(R.id.signButton)
    Button mButtonSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_in);


        ButterKnife.bind(this);




        /* adapt the image to the size of the display */
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                getResources(),R.drawable.background),size.x,size.y,true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mains = (RelativeLayout) findViewById(R.id.mains);


        Drawable d = new BitmapDrawable(getResources(), bmp);
        mains.setBackground(d);

        forgot = (TextView) findViewById(R.id.forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GetInActivity.this,ForgotPassword.class));
            }
        });
        signup_layout = (LinearLayout) findViewById(R.id.signup_layout) ;
        signup = (TextView) findViewById(R.id.signup);
        signup_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GetInActivity.this,SignUpActivity.class));
            }
        });


    }


    @OnClick(R.id.signButton)
    public void doLogin()
    {
        performLogin(mEditTextEmail.getText().toString().trim(),mEditTextPassword.getText().toString().trim());

    }

    //TODO define method for validating all fields
    private boolean validateFields() {
        Log.e("Email", mEditTextEmail.getText().toString());

        if ((TextUtils.isEmpty(mEditTextEmail.getText().toString().trim()))) {
            mEditTextEmail.setError(getString(R.string.empty_email));
            mEditTextEmail.requestFocus();
            return false;
        }
        if (!Utils.validateEmailPattern(mEditTextEmail.getText().toString().trim())) {
            mEditTextEmail.setError(getString(R.string.invalid_email));
            mEditTextEmail.requestFocus();
            return false;
        } else {
            if ((TextUtils.isEmpty(mEditTextPassword.getText().toString().trim()))) {
                mEditTextPassword.setError(getString(R.string.empty_password));
                mEditTextPassword.requestFocus();
                return false;
            }
            return true;
        }
    }


    private void performLogin(String email, String password) {
        if (validateFields()) {
            if (Utils.isNetworkAvailable(this)) {
                showLoader(GetInActivity.this,"Please wait...");
                LoginRequest loginRequest=new LoginRequest();
                loginRequest.setUsername(email);
                loginRequest.setPassword(password);
                FSApplication.getRestClient().doLogin(new StringRequestCallback<LoginResponse>() {
                    @Override
                    public void onRestResponse(Exception e, LoginResponse result) {
                        if (e == null && result != null) {
                            if(result.getStatus().equals("true"))
                            {
                                AppPrefrence.getInstance().saveUserId(GetInActivity.this,result.getResult().getUserid());
                                startActivity(new Intent(GetInActivity.this,DrawerActivity.class));
                                finish();
                            }
                            else
                             {
                                Toast.makeText(GetInActivity.this,result.getMessage(),Toast.LENGTH_SHORT).show();
                            }


                        }
                        hideLoader();

                    }

                }, loginRequest);
            } else {
                Toast.makeText(GetInActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

            }
        }

    }


}
