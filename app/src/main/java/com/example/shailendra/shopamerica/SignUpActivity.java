package com.example.shailendra.shopamerica;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.common.BaseActivity;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.country_list.CountryResponse;
import com.example.shailendra.shopamerica.rest_api.model.registration.RegisterResponse;
import com.example.shailendra.shopamerica.rest_api.model.registration.RegistrationRequest;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {
    private RelativeLayout main;

    ArrayAdapter<String> country_adapter;
    String[] country_array;
    String[] country_id_array;

    @Bind(R.id.email)
    AutoCompleteTextView mTextViewEmail;

    @Bind(R.id.password)
    EditText mEditTextPassword;

    @Bind(R.id.confirmpassword)
    EditText mEditTextConfirmPassword;

    @Bind(R.id.userlastname)
    EditText mEditTextUserLastName;

    @Bind(R.id.username)
    EditText mEditTextUserName;

    @Bind(R.id.signupbtn)
    Button mButtonSignUp;

    @Bind(R.id.spinnerCountry)
    fr.ganfra.materialspinner.MaterialSpinner spinnerCountry;

    @Bind(R.id.spinnerPlan)
    fr.ganfra.materialspinner.MaterialSpinner spinnerPlan;

    @Bind(R.id.etMobileNumber)
    EditText etMobileNumber;

    @Bind(R.id.etAddress)
    EditText etAddress;

    @Bind(R.id.etCity)
    EditText etCity;

    @Bind(R.id.etState)
    EditText etState;

    @Bind(R.id.etPostalCode)
    EditText etPostalCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

          /*Action Bar title and back button set*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.actionbar, null), new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.tcActivityTitle);
        titleTxtView.setText("Sign up");

        /*************************End**************************************************************************/

        addPlans();
        getCountryList();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void addPlans() {
        String[] ITEMS = {"Basic", "Premium", "Business"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPlan.setAdapter(adapter);
    }


    @OnClick(R.id.signupbtn)
    public void performSignup() {
        hideSoftKeyboard();
        if (validateFields()) {
            performRegisterUser();
        }
    }

    private void performRegisterUser() {

        if (validateFields()) {
            if (Utils.isNetworkAvailable(this)) {
                showLoader(SignUpActivity.this, "Please wait...");
                RegistrationRequest registrationRequest = new RegistrationRequest();

                registrationRequest.setEmail(mTextViewEmail.getText().toString().trim());
                registrationRequest.setName(mEditTextUserName.getText().toString().trim());
                registrationRequest.setLastname(mEditTextUserLastName.getText().toString().trim());
                registrationRequest.setMobileno(etMobileNumber.getText().toString().trim());
                registrationRequest.setAddress(etAddress.getText().toString().trim());
                registrationRequest.setCity(etCity.getText().toString().trim());
                registrationRequest.setState(etState.getText().toString().trim());
                registrationRequest.setPostalcode(etPostalCode.getText().toString().trim());
                registrationRequest.setPassword(mEditTextPassword.getText().toString().trim());
                registrationRequest.setConfirmPassword(mEditTextConfirmPassword.getText().toString().trim());
                registrationRequest.setPassword(mEditTextPassword.getText().toString().trim());
                registrationRequest.setPlan(spinnerPlan.getSelectedItem().toString().trim());
                registrationRequest.setCountry(spinnerCountry.getSelectedItem().toString().trim());
                registrationRequest.setUsername(mTextViewEmail.getText().toString().trim());


                FSApplication.getRestClient().doRegistration(new StringRequestCallback<RegisterResponse>() {
                    @Override
                    public void onRestResponse(Exception e, RegisterResponse result) {
                        if (e == null && result != null) {
                            if(result.getUserid().length()>0)
                            {
                                AppPrefrence.getInstance().saveUserId(SignUpActivity.this,result.getUserid());
                                startActivity(new Intent(SignUpActivity.this,DrawerActivity.class));
                                finish();
                            }
                            else
                            {
                                Toast.makeText(SignUpActivity.this,result.getMessage(),Toast.LENGTH_SHORT).show();
                            }

                        }
                        hideLoader();

                    }

                }, registrationRequest);
            } else {
                Toast.makeText(SignUpActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

            }
        }
    }


    private void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private boolean validateFields() {

        if(spinnerCountry.getSelectedItemPosition() ==0)
        {

            //spinnerCountry.setError(getString(R.string.country_blank));
            Toast.makeText(SignUpActivity.this,getString(R.string.country_blank),Toast.LENGTH_SHORT).show();
            spinnerCountry.requestFocus();
            return false;
        }
        if(spinnerPlan.getSelectedItemPosition() ==0)
        {

            //spinnerPlan.setError(getString(R.string.plan_blank));
            Toast.makeText(SignUpActivity.this,getString(R.string.plan_blank),Toast.LENGTH_SHORT).show();
            spinnerPlan.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty((mEditTextUserName.getText().toString().trim()))) {
            mEditTextUserName.setError(getString((R.string.signup_name_blank)));
            mEditTextUserName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty((mEditTextUserLastName.getText().toString().trim()))) {
            mEditTextUserLastName.setError(getString((R.string.signup_last_name_blank)));
            mEditTextUserLastName.requestFocus();
            return false;
        }
        if ((TextUtils.isEmpty(mTextViewEmail.getText().toString().trim()))) {
            mTextViewEmail.setError(getString(R.string.signup_email_blank));
            mTextViewEmail.requestFocus();
            return false;
        }
        if (!Utils.validateEmailPattern(mTextViewEmail.getText().toString().trim())) {
            mTextViewEmail.setError(getString(R.string.signup_email_invalid));
            mTextViewEmail.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etMobileNumber.getText().toString().trim())) {
            etMobileNumber.setError(getString(R.string.mobile_number_blank));
            etMobileNumber.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            etAddress.setError(getString(R.string.address_blank));
            etAddress.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
            etCity.setError(getString(R.string.city_blank));
            etCity.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etState.getText().toString().trim())) {
            etState.setError(getString(R.string.state_blank));
            etState.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etPostalCode.getText().toString().trim())) {
            etPostalCode.setError(getString(R.string.postal_code_blank));
            etPostalCode.requestFocus();
            return false;
        }

        if ((TextUtils.isEmpty(mEditTextPassword.getText().toString().trim()))) {
            mEditTextPassword.setError(getString(R.string.signup_password_blank));
            mEditTextPassword.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEditTextConfirmPassword.getText().toString().trim())) {
            mEditTextConfirmPassword.setError(getString(R.string.signup_confirm_password_blank));
            mEditTextConfirmPassword.requestFocus();
            return false;
        }
        if (!mEditTextPassword.getText().toString().equals(mEditTextConfirmPassword.getText().toString())) {
            mEditTextConfirmPassword.setError(getString(R.string.signup_password_not_matched));
            mEditTextConfirmPassword.requestFocus();
            return false;
        }
        return true;

    }


    private void getCountryList() {

        if (Utils.isNetworkAvailable(this)) {
            showLoader(SignUpActivity.this, "Fetching Countries...");
            FSApplication.getRestClient().getCountryList(new StringRequestCallback<CountryResponse>() {
                @Override
                public void onRestResponse(Exception e, CountryResponse result) {
                    if (e == null && result != null) {
                        if (result.getStatus().equals("true")) {
                            country_array = new String[result.getResult().length];
                            country_id_array = new String[result.getResult().length];


                            for (int i = 0; i < result.getResult().length; i++) {
                                country_array[i] = result.getResult()[i].getCountry_name();
                                country_id_array[i] = result.getResult()[i].getCountry_id();
                            }
                            country_adapter = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_item, country_array);
                            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerCountry.setAdapter(country_adapter);
                        } else {
                            Toast.makeText(SignUpActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                    hideLoader();

                }

            });
        } else {
            Toast.makeText(SignUpActivity.this, "No Internet Available", Toast.LENGTH_LONG).show();

        }


    }

}
