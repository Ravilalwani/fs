package com.example.shailendra.shopamerica;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.example.shailendra.shopamerica.utils.AppPrefrence;

/**
 * Created by daffodil on 3/6/16.
 */
public class SplashActivity extends Activity{

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if(AppPrefrence.getInstance().getUserId(SplashActivity.this) != null)
                {
                    Intent i = new Intent(SplashActivity.this, DrawerActivity.class);
                    startActivity(i);
                }else
                {
                    Intent i = new Intent(SplashActivity.this, GetInActivity.class);
                    startActivity(i);
                }






                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
