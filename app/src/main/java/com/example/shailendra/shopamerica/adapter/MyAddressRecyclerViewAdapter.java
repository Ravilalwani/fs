package com.example.shailendra.shopamerica.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.Result;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MyAddressRecyclerViewAdapter extends RecyclerView.Adapter<MyAddressRecyclerViewAdapter.ViewHolder> {

    private final List<Result> mValues;
    private Context mContext;
    private OnDeleteButtonClickedListener mOnDeleteButtonClickedListener;
    private OnEditButtonClickedListener mOnEditButtonClickedListener;
    public MyAddressRecyclerViewAdapter(List<Result> list,Context mContext,OnDeleteButtonClickedListener listener,OnEditButtonClickedListener listener2) {
        mValues = list;
        mOnDeleteButtonClickedListener=listener;
        mOnEditButtonClickedListener=listener2;
        this.mContext=mContext;
    }

    public void setOnBluetoothDeviceClickedListener(OnDeleteButtonClickedListener l) {
        mOnDeleteButtonClickedListener = l;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.bindData(position);

    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private int position;
        @Bind(R.id.btnDeleteAddress)
        ImageView btnDeleteAddress;

        @Bind(R.id.tvReceiptName)
        TextView tvReceiptName;

        @Bind(R.id.tvMobileNumber)
        TextView tvMobileNumber;

        @Bind(R.id.tvAddress)
        TextView tvAddress;

        @Bind(R.id.tvCityStatePincode)
        TextView tvCityStatePincode;

        @Bind(R.id.btnEditAddress)
        ImageView btnEditAddress;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

        }

        public void bindData(int positon)
        {
            this.position=positon;
        }

        @OnClick(R.id.btnDeleteAddress)
        public void onDeleteAddressClicked(View view){
            if (mOnDeleteButtonClickedListener != null) {
                final String addresss = mValues.get(position).getAddressId();
                mOnDeleteButtonClickedListener.OnDeleteButtonClicked(addresss,position);
            }
        }

        @OnClick(R.id.btnEditAddress)
        public void onEditAddressClicked(View view){
            if (mOnEditButtonClickedListener != null) {
                final String addresss = mValues.get(position).getAddressId();
                mOnEditButtonClickedListener.OnEditButtonClicked(addresss,position);
            }
        }


    }

    public interface OnDeleteButtonClickedListener {
        void OnDeleteButtonClicked(String addressid,int position);
    }

    public interface OnEditButtonClickedListener {
        void OnEditButtonClicked(String addressid,int position);
    }

}
