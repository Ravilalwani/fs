package com.example.shailendra.shopamerica.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shailendra.shopamerica.R;

import java.util.ArrayList;

/**
 * Created by daffodil on 6/7/16.
 */
public class MyPlansAdapter extends BaseAdapter {

    private ArrayList<String> mListLabel;
    private ArrayList<String> mListValues;
    private Context mContext;

    public MyPlansAdapter(Context mContext, ArrayList<String> mListLabel,ArrayList<String> mListValues)
    {
        this.mListLabel=mListLabel;
        this.mListValues=mListValues;
        this.mContext=mContext;
    }

    @Override
    public int getCount() {
        return mListLabel.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;
        if(convertView==null)
        {
            LayoutInflater layoutInflater=((Activity)mContext).getLayoutInflater();
            convertView=layoutInflater.inflate(R.layout.plans_item,parent,false);
            viewHolder=new ViewHolderItem();
            viewHolder.textViewLabel=(TextView) convertView.findViewById(R.id.label);
            viewHolder.textViewValue=(TextView) convertView.findViewById(R.id.value);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder=(ViewHolderItem) convertView.getTag();
        }

        viewHolder.textViewLabel.setText(mListLabel.get(position));
        viewHolder.textViewValue.setText(mListValues.get(position));

        return convertView;
    }

    static class ViewHolderItem {
        TextView textViewLabel;
        TextView textViewValue;
    }
}
