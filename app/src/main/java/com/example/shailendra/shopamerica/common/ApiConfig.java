package com.example.shailendra.shopamerica.common;

/**
 * Created by daffodil on 3/6/16.
 */
public class ApiConfig {
    public static final String BASE_URL = "http://demoserver.jbsoft.in/FS/mapp";

    public interface ApiBaseMethods
    {
        String LOGIN="login_confirm.php";
        String REGISTER="register_confim.php";
        String ALL_PLANS="member_plan.php";
        String COUNTRY_LIST="country_list.php";
        String FORGOT_PASSWORD="forgotpassword.php";
        String USER_PROFILE="profile.php";
        String WEIGHT_CONVERTER="converters.php";
        String MEMBER_PLANS="member_plan.php";
        String CHANGE_PASSWORD="change_password.php";
        String SHIPPING_RATES="shipping_rates.php";
        String TESTIMONIAL="testimonial.php";
        String EDIT_PROFILE="edit_profile.php";
        String UPLOAD_PROFILE_PIC="upload_profilepic.php";
        String SHIPPING_ADDRESS_LIST="list_shpping.php";
        String ADD_SHIPPING_ADDRESS="add_shipaddress.php";
        String DELETE_SHIPPING_ADDRESS="del_address.php";
    }

    public interface ErrorCodes {
        int SERVER_ERROR = 500;
    }
}
