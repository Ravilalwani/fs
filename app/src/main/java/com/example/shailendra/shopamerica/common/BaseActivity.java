package com.example.shailendra.shopamerica.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by daffodil on 3/6/16.
 */
public class BaseActivity extends AppCompatActivity {

    private ProgressDialog loading = null;

    public void showLoader(Context mContext,String message)
    {
        loading = new ProgressDialog(mContext);
        loading.setCancelable(false);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setMessage(message);
        loading.show();
    }


    public void hideLoader()
    {
        if(loading !=null)
        {
            loading.dismiss();
        }
    }
}
