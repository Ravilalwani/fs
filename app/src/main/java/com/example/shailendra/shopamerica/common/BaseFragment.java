package com.example.shailendra.shopamerica.common;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    private ProgressDialog loading = null;

    public void showLoader(Context mContext, String message)
    {
        if(loading == null)
        loading = new ProgressDialog(mContext);
        loading.setCancelable(false);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setMessage(message);
        loading.show();
    }


    public void hideLoader()
    {
        if(loading !=null)
        {
            loading.dismiss();
        }
    }

    public BaseFragment() {
        // Required empty public constructor
    }




}
