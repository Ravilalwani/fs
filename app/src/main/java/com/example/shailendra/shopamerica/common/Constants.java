package com.example.shailendra.shopamerica.common;

/**
 * Created by daffodil on 3/6/16.
 */
public class Constants {

    public static final String NO_RESPONSE = "No response from server";
    public static final String REQUEST_TIME_OUT = "Request time out";
    public static final String MESSAGE_SERVER_ERROR = "Server Error, Please try later";


    public interface LoginParams {

        public String USER_NAME = "Username";
        public String PASSWORD = "Password";

    }

    public interface SignupParams {
        public String PLAN = "Plan";
        public String NAME = "Name";
        public String EMAIL = "Email";
        public String PASSWORD = "Password";
        public String MOBILE_NUMBER = "Mobileno";
        public String ADDRESS = "Address";
        public String CITY = "City";
        public String POSTAL_CODE = "Postalcode";
        public String STATE = "State";
        public String USER_NAME = "Username";
        public String COUNTRY = "Country";
        public String CONFIRM_PASSWORD = "ConfirmPassword";

    }

    public interface ForgotPasswordParams{
        public String EMAIL_ID="Emailid";
    }

    public interface ProfileParams{
        public String USER_ID="Userid";
    }

    public interface WeightCoverterParams{
        public String WEIGHT_CONVERTER="Weightconverter";
        public String UNIT="Unit";
        public String CONVERT_VALUE="Convertvalue";

        public String VOLUME_CONVERTER="Volumeconverter";
        public String LENGTH="Length";
        public String WIDTH="Width";
        public String HEIGHT="Height";
    }

    public interface ChangePasswordParams{
        public String USER_ID="Userid";
        public String OLD_PASSWORD="OldPassword";
        public String NEW_PASSWORD="NewPassword";
    }

    public interface ShippingRatesParams{
        public String COUNTRY="Country";
        public String CARRIER="Carrier";

    }

    public interface ShippingRatesTypes{
        public String DHL="DHL";
        public String FEDEX="FEDEX";
        public String USPS="USPS";
        public String OTHERS="Others";

    }

    public interface EditProfileParams {
        public String USER_ID="Userid";
        public String FIRST_NAME = "Firstname";
        public String MIDDLE_NAME = "Middlename";
        public String LAST_NAME = "Lastname";
        public String DOB = "Dob";
        public String GENDER = "Gender";
        public String EMAIL = "Email";
        public String ADDRESS1 = "Address1";
        public String ADDRESS2 = "Address2";
        public String CITY = "City";
        public String POSTAL_CODE = "Pincode";
        public String STATE = "State";
        public String COUNTRY = "Country";
        public String TAXID = "Taxid";
        public String COUNTRY_CODE1 = "Countrycode1";
        public String LOCAL_CODE1 = "Localcode1";
        public String PHONE1 = "Phno1";
        public String COUNTRY_CODE2 = "Countrycode2";
        public String LOCAL_CODE2 = "Localcode2";
        public String PHONE2 = "Phno2";
        public String COUNTRY_CODE3 = "Countrycode3";
        public String MOBILE_NUMBER = "Mobile_no";
        public String FAX_NUMBER = "Faxno";
        public String USER_NAME = "Username";
        public String COMPANY = "Company";


    }

    public interface AddhippingAddressParams {
        public String USER_ID = "Userid";
        public String RECEIPT_NAME = "ReceiptName";
        public String ADDRESS = "Address";
        public String CITY = "City";
        public String POSTAL_CODE = "PinCode";
        public String STATE = "State";
        public String COUNTRY = "Country";
        public String DEFAULT="Default";
    }

    public interface deleteShippingAddress{
        public String USER_ID = "Userid";
        public String ADDRESS_ID = "AddressId";
    }
}
