package com.example.shailendra.shopamerica.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shailendra.shopamerica.AddShippingAddress;
import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.adapter.MyAddressRecyclerViewAdapter;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.Result;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.ShippingAddressResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddressFragment extends BaseFragment implements MyAddressRecyclerViewAdapter.OnDeleteButtonClickedListener,
MyAddressRecyclerViewAdapter.OnEditButtonClickedListener{


    @Bind(R.id.list)
    RecyclerView mRecyclerView;

    @Bind(R.id.fab)
    FloatingActionButton mFloatingActionButton;


    List<Result> mListAddress=new ArrayList<>();
    MyAddressRecyclerViewAdapter mAdapter;
    private Context mContext;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AddressFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_main, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.shipping_address));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter=new MyAddressRecyclerViewAdapter(mListAddress,mContext,this,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        getAddressData();

    }


    @OnClick(R.id.fab)
    public void addNewAddress()
    {
       startActivity(new Intent(getActivity(), AddShippingAddress.class));
    }



    private void getAddressData()
    {
        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(),"Please wait...");

            FSApplication.getRestClient().getShippingAddress(new StringRequestCallback<ShippingAddressResponse>() {
                @Override
                public void onRestResponse(Exception e, ShippingAddressResponse result) {
                    if (e == null && result != null) {

                        if(result.getResult().length==0)
                        {
                            Toast.makeText(getActivity(),"No Shipping address found",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            for (int index = 0; index < result.getResult().length; index++) {
                                mListAddress.add(index, result.getResult()[index]);
                            }
                            mAdapter.notifyDataSetChanged();
                        }



                    }
                    hideLoader();

                }

            }, AppPrefrence.getInstance().getUserId(getActivity()));
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }



    @Override
    public void OnDeleteButtonClicked(String addressid, final int position) {
        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(),"Please wait...");

            FSApplication.getRestClient().deleteShippingAddress(new StringRequestCallback<ShippingAddressResponse>() {
                @Override
                public void onRestResponse(Exception e, ShippingAddressResponse result) {
                    if (e == null && result != null) {

                        Toast.makeText(getActivity(),result.getMessage(),Toast.LENGTH_SHORT).show();
                        mListAddress.remove(position);
                        mAdapter.notifyDataSetChanged();

                    }
                    hideLoader();

                }

            }, AppPrefrence.getInstance().getUserId(getActivity()),addressid);
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void OnEditButtonClicked(String addressid, int position) {
      /*  Intent i=new Intent(getActivity(), AddShippingAddress.class);
        Bundle args = new Bundle();
        args.putParcelableArrayList("ARRAYLIST",mListAddress);
        args.putString("type","edit");
        i.putExtras(args);
        startActivity(i);*/
    }
}
