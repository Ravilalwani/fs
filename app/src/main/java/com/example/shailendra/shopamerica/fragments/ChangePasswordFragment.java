package com.example.shailendra.shopamerica.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.forgot_password.ForgotPasswordResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment {

    @Bind(R.id.etCurrentPassword)
    EditText etCurrentPassword;

    @Bind(R.id.etNewPassword)
    EditText etNewPassword;

    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Bind(R.id.btnChangePassword)
    Button btnChangePassword;



    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();//This removes all menu items (no need to know the id of each of them)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this,view);
        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.change_password));

        return view;
    }


    @OnClick(R.id.btnChangePassword)
    public void doChnangePassword()
    {
        if(validateFields())
        {
            if (Utils.isNetworkAvailable(getActivity())) {
                showLoader(getActivity(),"Please wait...");

                String user_id= AppPrefrence.getInstance().getUserId(getActivity());
                String old_password=etCurrentPassword.getText().toString().trim();
                String new_password=etNewPassword.getText().toString().trim();

                FSApplication.getRestClient().changePassword(new StringRequestCallback<ForgotPasswordResponse>() {
                    @Override
                    public void onRestResponse(Exception e, ForgotPasswordResponse result) {
                        if (e == null && result != null) {
                            etCurrentPassword.setText("");
                            etNewPassword.setText("");
                            etConfirmPassword.setText("");
                            Toast.makeText(getActivity(),result.getMessage(),Toast.LENGTH_SHORT).show();

                        }
                        hideLoader();

                    }

                }, user_id,old_password,new_password);
            } else {
                Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

            }
        }

    }

    private boolean validateFields()
    {
        if ((TextUtils.isEmpty(etCurrentPassword.getText().toString().trim()))) {
            etCurrentPassword.setError(getString(R.string.current_password_blank));
            etCurrentPassword.requestFocus();
            return false;
        }
        if ((TextUtils.isEmpty(etNewPassword.getText().toString().trim()))) {
            etNewPassword.setError(getString(R.string.new_password_blank));
            etNewPassword.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(etConfirmPassword.getText().toString().trim())) {
            etConfirmPassword.setError(getString(R.string.retype_new_blank));
            etConfirmPassword.requestFocus();
            return false;
        }
        if (!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            etConfirmPassword.setError(getString(R.string.new_password_not_matched));
            etConfirmPassword.requestFocus();
            return false;
        }
        return true;
    }

}
