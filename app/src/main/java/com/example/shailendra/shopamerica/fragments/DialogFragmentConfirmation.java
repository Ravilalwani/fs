package com.example.shailendra.shopamerica.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.shailendra.shopamerica.R;

/**
 * Created by daffodil on 6/7/16.
 */
public class DialogFragmentConfirmation extends DialogFragment {

    private Context mContext;
    private LayoutInflater mInflater;
    static String planType1;


    public static DialogFragmentConfirmation newInstance(String number,String planType) {

        planType1=planType;
        DialogFragmentConfirmation fragment = new DialogFragmentConfirmation();

        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Resources resources = getResources();
            int padding = resources.getDimensionPixelOffset(R.dimen.medium_margins);
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
//            if (fillHeight()) {
//                height = ViewGroup.LayoutParams.MATCH_PARENT;
//            }
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            dialog.getWindow().setLayout((int) (metrics.widthPixels * 0.90f), height);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int parentLayoutId = R.layout.dragment_dialog;
        if (parentLayoutId != -1) {
            View rootView = inflater.inflate(parentLayoutId, container, false);
            mContext = rootView.getContext();
            mInflater = LayoutInflater.from(mContext);
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog().setCanceledOnTouchOutside(true);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
            setCancelable(true);
            return rootView;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Resources res = mContext.getResources();
        String plan = String.format(res.getString(R.string.plan_message), planType1);

        TextView message = (TextView) view.findViewById(R.id.message);
        message.setText(plan);
        Bundle bundle = getArguments();


        Button yes = (Button) view.findViewById(R.id.yes);
        if (yes != null) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
        Button no = (Button) view.findViewById(R.id.no);
        if (no != null) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
    }


}
