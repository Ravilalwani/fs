package com.example.shailendra.shopamerica.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.weight_converter.ConverterRequest;
import com.example.shailendra.shopamerica.rest_api.model.weight_converter.WeightConverterResponse;
import com.example.shailendra.shopamerica.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentConverters extends BaseFragment {

    private Context mContext;

    @Bind(R.id.kilogramTab)
    RadioButton kilogramTab;

    @Bind(R.id.PoundTab)
    RadioButton PoundTab;

    @Bind(R.id.etWeight)
    EditText etWeight;

    @Bind(R.id.btnConvertWeight)
    Button btnConvertWeight;

    @Bind(R.id.btnResetWeight)
    Button btnResetWeight;

    @Bind(R.id.tvPounds)
    TextView tvPounds;

    @Bind(R.id.tvKilograms)
    TextView tvKilograms;

    @Bind(R.id.etLength)
    EditText etLength;

    @Bind(R.id.etWidth)
    EditText etWidth;

    @Bind(R.id.etHeight)
    EditText etHeight;

    @Bind(R.id.tvVolumeConverted)
    TextView tvVolumeConverted;

    @Bind(R.id.btnConvertVolume)
    Button btnConvertVolume;


    public FragmentConverters() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();//This removes all menu items (no need to know the id of each of them)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_converters, container, false);
        ButterKnife.bind(this, view);
        mContext = getActivity();
        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.converters));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resetWeightValues();
        resetVolumeValues();
    }

    /********************************************
     * Weight Converter
     ****************************************************************/

    @OnClick(R.id.btnResetWeight)
    public void resetWeight() {
        resetWeightValues();
    }


    private void resetWeightValues() {
        Resources res = mContext.getResources();
        String kg = String.format(res.getString(R.string.converted_value_in_kilogram), 0);
        String pounds = String.format(res.getString(R.string.converted_value_in_pound), 0);
        tvKilograms.setText(kg);
        tvPounds.setText(pounds);
        etWeight.setText("");
    }

    @OnClick(R.id.btnConvertWeight)
    public void convertWeight() {
        if (etWeight.getText().toString().trim().length() > 0) {
            getConvertedValue();

        } else {
            etWeight.setError(getString(R.string.unit_blank));
            etWeight.requestFocus();
        }
    }


    private void getConvertedValue() {
        if (Utils.isNetworkAvailable(getActivity())) {

            if (kilogramTab.isChecked()) {
                showLoader(getActivity(), "Converting Kilograms to Pound...");
            } else {

                showLoader(getActivity(), "Converting Pound to Kilograms...");

            }


            ConverterRequest request = new ConverterRequest();
            request.setWeightconverter("Yes");
            request.setVolumeconverter("No");
            request.setConvertvalue(etWeight.getText().toString().trim());

            if (kilogramTab.isChecked()) {
                request.setUnit("Kilogram");
            } else {
                request.setUnit("Pound");
            }


            FSApplication.getRestClient().weightConverter(new StringRequestCallback<WeightConverterResponse>() {
                @Override
                public void onRestResponse(Exception e, WeightConverterResponse result) {
                    if (e == null && result != null) {

                        Resources res = mContext.getResources();
                        String kg = String.format(res.getString(R.string.converted_value_in_kilogram), result.getResult().getKilogram());
                        tvKilograms.setText(kg);

                        String pounds = String.format(res.getString(R.string.converted_value_in_pound), result.getResult().getPound());
                        tvPounds.setText(pounds);


                    }
                    hideLoader();

                }

            }, request);
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }

    /********************************************
     * Volume Converter
     ****************************************************************/


    private void resetVolumeValues() {

        Resources res = mContext.getResources();
        String volume = String.format(res.getString(R.string.total_dimensions_weight), 0);
       tvVolumeConverted.setText(volume);
    }

    @OnClick(R.id.btnConvertVolume)
    public void convertVolume() {

        if (etLength.getText().toString().trim().length() == 0) {
            etLength.setError(mContext.getString(R.string.length_blank));
            etLength.requestFocus();
        }else
        if (etWidth.getText().toString().trim().length() == 0) {
            etWidth.setError(mContext.getString(R.string.width_blank));
            etWidth.requestFocus();
        } else if (etHeight.getText().toString().trim().length() == 0) {
            etHeight.setError(mContext.getString(R.string.height_blank));
            etHeight.requestFocus();

        } else  {

            convertInVolume();
        }
    }


    private void convertInVolume() {
        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(), "Converting in volume...");

            ConverterRequest request = new ConverterRequest();
            request.setVolumeconverter("Yes");
            request.setWeightconverter("No");
            request.setHeight(etHeight.getText().toString().trim());
            request.setLength(etLength.getText().toString().trim());
            request.setWidth(etWidth.getText().toString().trim());

            FSApplication.getRestClient().weightConverter(new StringRequestCallback<WeightConverterResponse>() {
                @Override
                public void onRestResponse(Exception e, WeightConverterResponse result) {
                    if (e == null && result != null) {

                        Resources res = mContext.getResources();
                        String[] volume=result.getResult().getDimensional_Weight_Results().split(" ");
                        String original_volume = String.format(res.getString(R.string.total_dimensions_weight), volume[0]);
                        tvVolumeConverted.setText(original_volume);


                    }
                    hideLoader();

                }

            }, request);
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }

}
