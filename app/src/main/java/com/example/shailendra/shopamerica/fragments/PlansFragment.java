package com.example.shailendra.shopamerica.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.adapter.MyPlansAdapter;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.my_plans.MyPlansResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlansFragment extends BaseFragment {

    private static final java.lang.String TAG ="" ;
    ArrayList<String> mListPlans=new ArrayList<>();
    ArrayList<String> mListValues=new ArrayList<>();
    MyPlansResponse mResponse;

    MyPlansAdapter mAdapter;
    private Context mContext;

    @Bind(R.id.basicTab)
    RadioButton mRadioButtonBasic;

    @Bind(R.id.premiumTab)
    RadioButton mRadioButtonPremium;

    @Bind(R.id.businessTab)
    RadioButton mRadioButtonBusiness;

    @Bind(R.id.selectButton)
    Button selectButton;

    @Bind(R.id.plansList)
    ListView plansList;

    @Bind(R.id.plansRadioGroup)
    RadioGroup plansRadioGroup;





    public PlansFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();//This removes all menu items (no need to know the id of each of them)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mContext=getActivity();
        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.Member_Plan));
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this,view);



        /*Dummy data */
        String[] plans_array = getResources().getStringArray(R.array.plans_label);
        String[] values_array = getResources().getStringArray(R.array.plans_values);
        mListPlans.addAll(Arrays.asList(plans_array));
        mListValues.addAll(Arrays.asList(values_array));

        mAdapter=new MyPlansAdapter(mContext,mListPlans,mListValues);
        plansList.setAdapter(mAdapter);

        getPlansList();


        plansRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.basicTab:
                        setBasicValues();
                        mAdapter.notifyDataSetChanged();
                        plansList.setSelectionAfterHeaderView();
                        break;
                    case R.id.premiumTab:
                        setPremiumValues();
                        mAdapter.notifyDataSetChanged();
                        plansList.setSelectionAfterHeaderView();
                        break;
                    case R.id.businessTab:
                        setBusinessValues();
                        mAdapter.notifyDataSetChanged();
                        plansList.setSelectionAfterHeaderView();
                        break;

                }
            }
        });

    }

    private void setBusinessValues() {

        mListValues.clear();
        mListValues.add(mResponse.getResult().getBusiness().getOne_Time_Set_upFee());
        mListValues.add(mResponse.getResult().getBusiness().getMonthly_Fee());
        mListValues.add(mResponse.getResult().getBusiness().getPersonal_US_Address());
        mListValues.add(mResponse.getResult().getBusiness().getConsolidation_Services());
        mListValues.add(mResponse.getResult().getBusiness().getShipping_Services_to_223_countries());
        mListValues.add(mResponse.getResult().getBusiness().getRepacking_Services());
        mListValues.add(mResponse.getResult().getBusiness().getDelivery_Destinations());
        mListValues.add(mResponse.getResult().getBusiness().getPersonal_Shopping_Service_Fee());
        mListValues.add(mResponse.getResult().getBusiness().getImaging_Services());
        mListValues.add(mResponse.getResult().getBusiness().getInbound_Packages());
        mListValues.add(mResponse.getResult().getBusiness().getDedicated_Email_Helpdesk());
        mListValues.add(mResponse.getResult().getBusiness().getStorage());
        mListValues.add(mResponse.getResult().getBusiness().getShipping_Discounts());
        mListValues.add(mResponse.getResult().getBusiness().getCurrent_Plan());
    }

    private void setPremiumValues() {

        mListValues.clear();
        mListValues.add(mResponse.getResult().getPremium().getOne_Time_Set_upFee());
        mListValues.add(mResponse.getResult().getPremium().getMonthly_Fee());
        mListValues.add(mResponse.getResult().getPremium().getPersonal_US_Address());
        mListValues.add(mResponse.getResult().getPremium().getConsolidation_Services());
        mListValues.add(mResponse.getResult().getPremium().getShipping_Services_to_223_countries());
        mListValues.add(mResponse.getResult().getPremium().getRepacking_Services());
        mListValues.add(mResponse.getResult().getPremium().getDelivery_Destinations());
        mListValues.add(mResponse.getResult().getPremium().getPersonal_Shopping_Service_Fee());
        mListValues.add(mResponse.getResult().getPremium().getImaging_Services());
        mListValues.add(mResponse.getResult().getPremium().getInbound_Packages());
        mListValues.add(mResponse.getResult().getPremium().getDedicated_Email_Helpdesk());
        mListValues.add(mResponse.getResult().getPremium().getStorage());
        mListValues.add(mResponse.getResult().getPremium().getShipping_Discounts());
        mListValues.add(mResponse.getResult().getPremium().getCurrent_Plan());
    }

    private void setBasicValues() {

        mListValues.clear();
        mListValues.add(mResponse.getResult().getBasic().getOne_Time_Set_upFee());
        mListValues.add(mResponse.getResult().getBasic().getMonthly_Fee());
        mListValues.add(mResponse.getResult().getBasic().getPersonal_US_Address());
        mListValues.add(mResponse.getResult().getBasic().getConsolidation_Services());
        mListValues.add(mResponse.getResult().getBasic().getShipping_Services_to_223_countries());
        mListValues.add(mResponse.getResult().getBasic().getRepacking_Services());
        mListValues.add(mResponse.getResult().getBasic().getDelivery_Destinations());
        mListValues.add(mResponse.getResult().getBasic().getPersonal_Shopping_Service_Fee());
        mListValues.add(mResponse.getResult().getBasic().getImaging_Services());
        mListValues.add(mResponse.getResult().getBasic().getInbound_Packages());
        mListValues.add(mResponse.getResult().getBasic().getDedicated_Email_Helpdesk());
        mListValues.add(mResponse.getResult().getBasic().getStorage());
        mListValues.add(mResponse.getResult().getBasic().getShipping_Discounts());
        mListValues.add(mResponse.getResult().getBasic().getCurrent_Plan());
    }

    @OnClick(R.id.selectButton)
    public void onSubmit()
    {
        String plantype;
        if(mRadioButtonBasic.isChecked())
        {
            plantype="Basic";
        }else if(mRadioButtonBusiness.isChecked())
        {
            plantype="Business";
        }else{
            plantype="Premium";
        }

        DialogFragmentConfirmation confirmationDialog = DialogFragmentConfirmation.newInstance("",plantype);
        confirmationDialog.show(getChildFragmentManager(), "Dialog");
    }

    private void getPlansList()
    {
        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(),"Please wait...");

            FSApplication.getRestClient().getPlans(new StringRequestCallback<MyPlansResponse>() {
                @Override
                public void onRestResponse(Exception e, MyPlansResponse result) {
                    if (e == null && result != null) {

                        mResponse=result;
                        mRadioButtonBasic.setChecked(true);
                        //mRadioButtonBasic.setChecked(true);



                    }
                    hideLoader();

                }

            }, AppPrefrence.getInstance().getUserId(getActivity()));
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }

    }
}
