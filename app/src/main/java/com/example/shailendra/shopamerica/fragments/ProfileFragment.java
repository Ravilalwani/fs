package com.example.shailendra.shopamerica.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.RequestCallback;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.ErrorModel;
import com.example.shailendra.shopamerica.rest_api.model.edit_profile.EditProfileResponse;
import com.example.shailendra.shopamerica.rest_api.model.user_profile.FilePart;
import com.example.shailendra.shopamerica.rest_api.model.user_profile.ProfileResponse;
import com.example.shailendra.shopamerica.utils.AppPrefrence;
import com.example.shailendra.shopamerica.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


public class ProfileFragment extends BaseFragment {



    private LinearLayout profmain;
    private LinearLayout contentLinear;

    @Bind(R.id.profileImageView)
    de.hdodenhof.circleimageview.CircleImageView mImageViewProfile;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.tvCountry)
    TextView tvCountry;

    @Bind(R.id.tvEmail)
    TextView tvEmail;


    @Bind(R.id.tvAddress)
    TextView tvAddress;


    @Bind(R.id.tvMobile)
    TextView tvMobile;

    @Bind(R.id.tvPhone)
    TextView tvPhone;

    public ProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        profmain = (LinearLayout) view.findViewById(R.id.profmain);
        contentLinear = (LinearLayout) view.findViewById(R.id.contentLinear);
        Drawable  dcontent = getResources().getDrawable(R.drawable.content_bg);
        contentLinear.setBackground(dcontent);

        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.my_profile));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        EasyImage.configuration(getActivity());
        getProfile();
    }

    private void getProfile() {
            if (Utils.isNetworkAvailable(getActivity())) {
                showLoader(getActivity(),"Please wait...");
                String userid=AppPrefrence.getInstance().getUserId(getActivity());

                FSApplication.getRestClient().getUserProfile(new StringRequestCallback<ProfileResponse>() {
                    @Override
                    public void onRestResponse(Exception e, ProfileResponse result) {
                        if (e == null && result != null) {

                            AppPrefrence.getInstance().saveImageUrl(getActivity(),result.getResult().getProfileImg());
                            AppPrefrence.getInstance().saveUserName(getActivity(),result.getResult().getFirst_Name());
                            Picasso.with(getActivity()).load(result.getResult().getProfileImg()).placeholder(R.drawable.prifile_dummy)
                                    .error(R.drawable.prifile_dummy)
                                    .into(mImageViewProfile);
                            tvUserName.setText(result.getResult().getFirst_Name());
                           // tvCountry.setText(result.getResult().getc());
                            tvEmail.setText(result.getResult().getEmail());
                            tvAddress.setText(result.getResult().getAddress());
                            tvMobile.setText(result.getResult().getPhone1());
                            tvPhone.setText(result.getResult().getPhone2());



                        }
                        hideLoader();

                    }

                },userid);
            } else {
                Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

            }

    }

    @OnClick(R.id.profileImageView)
    public void getProfilePicture()
    {
        EasyImage.openChooserWithGallery(this, "Choose profile image", 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                onPhotoReturned(imageFile);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getActivity());
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotoReturned(final File photoFile) {


        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(), "Uploading profile picture...");
            String userid = AppPrefrence.getInstance().getUserId(getActivity());
            FilePart filePart = new FilePart(photoFile, "image/jpg");

            FSApplication.getRestClient().uploadProfileImage(new RequestCallback<EditProfileResponse>() {
                @Override
                public void onRestResponse(Exception e, EditProfileResponse result) {

                    Toast.makeText(getActivity(),result.getMessage(),Toast.LENGTH_SHORT).show();

                    Picasso.with(getActivity()).load(photoFile)
                            .into(mImageViewProfile);

                    hideLoader();

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    Toast.makeText(getActivity(),result.getMessage(),Toast.LENGTH_SHORT).show();
                    hideLoader();

                }
            }, filePart, userid);


        }


    }

    @Override
    public void onDestroy() {
        // Clear any configuration that was done!
        EasyImage.clearConfiguration(getActivity());
        super.onDestroy();
    }

}
