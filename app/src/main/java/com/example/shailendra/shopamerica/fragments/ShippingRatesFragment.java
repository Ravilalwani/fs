package com.example.shailendra.shopamerica.fragments;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.common.Constants;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.country_list.CountryResponse;
import com.example.shailendra.shopamerica.rest_api.model.shipping_rates_response.ShippingRatesResponse;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShippingRatesFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener{

    @Bind(R.id.tableLayout)
     TableLayout mTableLayout;
    private String[] country_array;
    private String[] country_id_array;
    ArrayAdapter<String> country_adapter;
    @Bind(R.id.spinnerCountry)
    fr.ganfra.materialspinner.MaterialSpinner mSpinner;

    @Bind(R.id.radioGroupPlan)
    RadioGroup radioGroupPlan;

    @Bind(R.id.radioBtnDHL)
    RadioButton radioBtnDHL;

    @Bind(R.id.radioBtnFEDEX)
    RadioButton radioBtnFEDEX;

    @Bind(R.id.radioBtnUSPS)
    RadioButton radioBtnUSPS;

    @Bind(R.id.radioBtnSpecial)
    RadioButton radioBtnSpecial;


    public ShippingRatesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();//This removes all menu items (no need to know the id of each of them)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_shipping_rates, container, false);

        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.shipping_rates));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        radioGroupPlan.setOnCheckedChangeListener(this);
        getCountryList();
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(mSpinner.getSelectedItemPosition()==0)
                {
                    //Toast.makeText(getActivity(),"Please select your country",Toast.LENGTH_SHORT).show();
                }
                else
                {

                    if(radioBtnDHL.isChecked())
                    {
                        getShippingRates(Constants.ShippingRatesTypes.DHL);
                    }else  if(radioBtnFEDEX.isChecked())
                    {
                        getShippingRates(Constants.ShippingRatesTypes.FEDEX);
                    }else  if(radioBtnUSPS.isChecked())
                    {
                        getShippingRates(Constants.ShippingRatesTypes.USPS);
                    }else{
                        getShippingRates(Constants.ShippingRatesTypes.DHL);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




    }

    private void getCountryList() {

        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(), "Fetching Countries...");
            FSApplication.getRestClient().getCountryList(new StringRequestCallback<CountryResponse>() {
                @Override
                public void onRestResponse(Exception e, CountryResponse result) {
                    if (e == null && result != null) {
                        if (result.getStatus().equals("true")) {
                            country_array = new String[result.getResult().length];
                            country_id_array = new String[result.getResult().length];


                            for (int i = 0; i < result.getResult().length; i++) {
                                country_array[i] = result.getResult()[i].getCountry_name();
                                country_id_array[i] = result.getResult()[i].getCountry_id();
                            }
                            country_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, country_array);
                            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            mSpinner.setAdapter(country_adapter);
                            String locale = getActivity().getResources().getConfiguration().locale.getDisplayCountry();
                            if(Arrays.asList(country_array).contains(locale))
                            {

                                mSpinner.setSelection(Arrays.asList(country_array).indexOf(locale)+1);
                            }

                           /* final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    radioBtnDHL.setChecked(true);
                                }
                            }, 200);*/


                        } else {
                            Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }


                }

            });
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }
    }



    public void getShippingRates(String type) {

            if (Utils.isNetworkAvailable(getActivity())) {
                showLoader(getActivity(),"Please wait...");

                FSApplication.getRestClient().shippingRates(new StringRequestCallback<ShippingRatesResponse>() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        super.onErrorResponse(error);
                        hideLoader();
                    }

                    @Override
                    public void onRestResponse(Exception e, ShippingRatesResponse result) {
                        if (e == null && result != null) {
                           // Toast.makeText(getActivity(),"done",Toast.LENGTH_SHORT).show();
                            while (mTableLayout.getChildCount() > 1)
                                mTableLayout.removeView(mTableLayout.getChildAt(mTableLayout.getChildCount() - 1));

                            for(int i=0;i<result.getResult().length;i++)
                            {
                                TableRow tr = new TableRow(getActivity());
                                tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 0,1));
                                tr.setPadding(0,10,0,0);


                                for(int j=0;j<4;j++)
                                {
                                    TextView t=new TextView(getActivity());

                                    TableRow.LayoutParams params = new TableRow.LayoutParams(
                                            0,
                                            TableRow.LayoutParams.WRAP_CONTENT,.2f
                                    );
                                    if(i==0)
                                    {
                                        params.setMargins(4,0,4,0);
                                    }
                                    else
                                    {
                                        params.setMargins(0,0,4,0);
                                    }
                                    t.setLayoutParams(params);

                                    t.setPadding(4,4,4,4);
                                    if(j==0)
                                    {
                                        t.setText(result.getResult()[i].getWeight());
                                    }else if(j==1)
                                    {
                                        t.setText(result.getResult()[i].getBasic());

                                    }else if(j==2)
                                    {
                                        t.setText(result.getResult()[i].getPremium());
                                    }else
                                    {
                                        t.setText(result.getResult()[i].getPremium());
                                    }

                                    t.setGravity(Gravity.CENTER);
                                    t.setBackgroundResource(R.color.colorBgCell);
                                    tr.addView(t);
                                }


                                mTableLayout.addView(tr);


                            }


                        }

                      hideLoader();

                    }


                }, mSpinner.getSelectedItem().toString(),type);
            } else {
                Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

            }

    }


    public class CreateView extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId)
        {
            case R.id.radioBtnDHL:
                getShippingRates("DHL");
                break;
            case R.id.radioBtnFEDEX:
                getShippingRates("FEDEX");
                break;
            case R.id.radioBtnUSPS:
                getShippingRates("USPS");
                break;
            case R.id.radioBtnSpecial:
                getShippingRates("Others");
                break;


        }
    }
}
