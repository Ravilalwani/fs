package com.example.shailendra.shopamerica.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shailendra.shopamerica.DrawerActivity;
import com.example.shailendra.shopamerica.FSApplication;
import com.example.shailendra.shopamerica.R;
import com.example.shailendra.shopamerica.adapter.TestimonialRecyclerAdapter;
import com.example.shailendra.shopamerica.common.BaseFragment;
import com.example.shailendra.shopamerica.rest_api.StringRequestCallback;
import com.example.shailendra.shopamerica.rest_api.model.testimonial.Result;
import com.example.shailendra.shopamerica.rest_api.model.testimonial.TestimonialResponse;
import com.example.shailendra.shopamerica.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestimonailFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    final ArrayList results = new ArrayList<Result>();

    public TestimonailFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(final Menu menu) {

        super.onPrepareOptionsMenu(menu);

        menu.clear();//This removes all menu items (no need to know the id of each of them)
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((DrawerActivity) getActivity()).setActionBarTitle(getString(R.string.testimonial));
        return inflater.inflate(R.layout.fragment_testimonail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new TestimonialRecyclerAdapter(results);
        mRecyclerView.setAdapter(mAdapter);
        getDataSet();
    }

    private void getDataSet() {


        if (Utils.isNetworkAvailable(getActivity())) {
            showLoader(getActivity(),"Please wait...");

            FSApplication.getRestClient().getTestimonial(new StringRequestCallback<TestimonialResponse>() {
                @Override
                public void onRestResponse(Exception e, TestimonialResponse result) {
                    if (e == null && result != null) {

                        for (int index = 0; index < result.getResult().length; index++) {
                            results.add(index, result.getResult()[index]);
                        }
                        mAdapter.notifyDataSetChanged();

                    }
                    hideLoader();

                }

            });
        } else {
            Toast.makeText(getActivity(), "No Internet Available", Toast.LENGTH_LONG).show();

        }



    }
}
