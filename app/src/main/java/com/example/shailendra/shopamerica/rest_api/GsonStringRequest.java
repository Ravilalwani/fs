package com.example.shailendra.shopamerica.rest_api;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 15-Apr-16.
 * al-oula-radio-app-android
 */
public class GsonStringRequest<K> extends StringRequest {


    protected static final String PROTOCOL_CHARSET = "utf-8";
//    protected static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    protected static final String PROTOCOL_CONTENT_TYPE = String.format("application/x-www-form-urlencoded; charset=%s", PROTOCOL_CHARSET);

    private final Map<String, String> headers;
    private final Map<String, String> params;

    public GsonStringRequest(int method, String url, StringRequestCallback<K> callback) {
        super(method, url,callback,callback);
        headers = new HashMap<>();
        params=new HashMap<>();
        initializeHeaders();
    }

    private void initializeHeaders() {
       putHeaders("Content-Type", "application/x-www-form-urlencoded");
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    public void putHeaders(String key, String value) {
        if (headers != null) {
            headers.put(key, value);
        }
    }

    public void putParams(String key, String value) {
        if (params != null) {
            params.put(key, value);
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}
