package com.example.shailendra.shopamerica.rest_api;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.shailendra.shopamerica.rest_api.model.ErrorModel;
import com.google.gson.Gson;


/**
 * Created by user on 12/8/2015.
 */
public abstract class RequestCallback<T> implements ResponseListener<T> {

    Gson gson = new Gson();

    @Override
    public void onSuccessResponse(T responseData) {
        onRestResponse(null, responseData);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        ErrorModel eModel = null;
        if (error.networkResponse != null && error.networkResponse.data != null) {
            try {
                if (error.networkResponse.headers != null) {
                    String errorString = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                    Gson gsonError = new Gson();
                    eModel = (ErrorModel) gsonError.fromJson(errorString, ErrorModel.class);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        onErrorResponse(error, eModel);
    }


    public abstract void onRestResponse(Exception e, T result);

    public abstract void onErrorResponse(Exception e, ErrorModel result);


}
