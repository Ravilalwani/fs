package com.example.shailendra.shopamerica.rest_api;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.example.shailendra.shopamerica.common.ApiConfig;
import com.example.shailendra.shopamerica.common.Constants;
import com.example.shailendra.shopamerica.rest_api.model.country_list.CountryResponse;
import com.example.shailendra.shopamerica.rest_api.model.edit_profile.EditProfileRequest;
import com.example.shailendra.shopamerica.rest_api.model.edit_profile.EditProfileResponse;
import com.example.shailendra.shopamerica.rest_api.model.forgot_password.ForgotPasswordResponse;
import com.example.shailendra.shopamerica.rest_api.model.login.LoginRequest;
import com.example.shailendra.shopamerica.rest_api.model.login.LoginResponse;
import com.example.shailendra.shopamerica.rest_api.model.my_plans.MyPlansResponse;
import com.example.shailendra.shopamerica.rest_api.model.registration.RegisterResponse;
import com.example.shailendra.shopamerica.rest_api.model.registration.RegistrationRequest;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.ShippingAddressChangeRequest;
import com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address.ShippingAddressResponse;
import com.example.shailendra.shopamerica.rest_api.model.shipping_rates_response.ShippingRatesResponse;
import com.example.shailendra.shopamerica.rest_api.model.testimonial.TestimonialResponse;
import com.example.shailendra.shopamerica.rest_api.model.user_profile.FilePart;
import com.example.shailendra.shopamerica.rest_api.model.user_profile.ProfileResponse;
import com.example.shailendra.shopamerica.rest_api.model.weight_converter.ConverterRequest;
import com.example.shailendra.shopamerica.rest_api.model.weight_converter.WeightConverterResponse;

/**
 * Created by user on 12/8/2015.
 */
public class RestClient {

    public VolleyManager getmVolleyManager() {
        return mVolleyManager;
    }

    protected static final String PROTOCOL_CHARSET = "utf-8";
    protected static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    VolleyManager mVolleyManager;

    public RestClient(Context appContext) {
        mVolleyManager = new VolleyManager(appContext);
    }


    public RequestHandler doLogin(final StringRequestCallback<LoginResponse> callback, LoginRequest request) {
        String loginUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.LOGIN)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, loginUrl, callback);
        callback.setClazz(LoginResponse.class);
        stringRequest.putParams(Constants.LoginParams.USER_NAME, request.getUsername());
        stringRequest.putParams(Constants.LoginParams.PASSWORD, request.getPassword());
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler doRegistration(final StringRequestCallback<RegisterResponse> callback, RegistrationRequest request) {
        String registerUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.REGISTER)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, registerUrl, callback);
        callback.setClazz(RegisterResponse.class);
        stringRequest.putParams(Constants.SignupParams.PLAN, request.getPlan());
        stringRequest.putParams(Constants.SignupParams.COUNTRY, request.getCountry());
        stringRequest.putParams(Constants.SignupParams.NAME, request.getName());
        stringRequest.putParams(Constants.SignupParams.EMAIL, request.getEmail());
        stringRequest.putParams(Constants.SignupParams.MOBILE_NUMBER, request.getMobileno());
        stringRequest.putParams(Constants.SignupParams.ADDRESS, request.getAddress());
        stringRequest.putParams(Constants.SignupParams.CITY, request.getCity());
        stringRequest.putParams(Constants.SignupParams.STATE, request.getState());
        stringRequest.putParams(Constants.SignupParams.POSTAL_CODE, request.getPostalcode());
        stringRequest.putParams(Constants.SignupParams.PASSWORD, request.getPassword());
        stringRequest.putParams(Constants.SignupParams.CONFIRM_PASSWORD, request.getConfirmPassword());
        stringRequest.putParams(Constants.SignupParams.USER_NAME, request.getEmail());
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler getCountryList(final StringRequestCallback<CountryResponse> callback) {
        String registerUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.COUNTRY_LIST)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.GET, registerUrl, callback);
        callback.setClazz(CountryResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler forgotPassword(final StringRequestCallback<ForgotPasswordResponse> callback,String email) {
        String forgotPasswordUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.FORGOT_PASSWORD)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, forgotPasswordUrl, callback);
        stringRequest.putParams(Constants.ForgotPasswordParams.EMAIL_ID,email);
        callback.setClazz(ForgotPasswordResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler getUserProfile(final StringRequestCallback<ProfileResponse> callback, String user_id) {
        String getProfileUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.USER_PROFILE)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, getProfileUrl, callback);
        stringRequest.putParams(Constants.ProfileParams.USER_ID,user_id);
        callback.setClazz(ProfileResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler weightConverter(final StringRequestCallback<WeightConverterResponse> callback, ConverterRequest request) {
        String weightConverterUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.WEIGHT_CONVERTER)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, weightConverterUrl, callback);
        if(request.getWeightconverter().equals("Yes"))
        {
            stringRequest.putParams(Constants.WeightCoverterParams.WEIGHT_CONVERTER,"Yes");
            stringRequest.putParams(Constants.WeightCoverterParams.UNIT,request.getUnit());
            stringRequest.putParams(Constants.WeightCoverterParams.CONVERT_VALUE,request.getConvertvalue());
        }else
        {
            stringRequest.putParams(Constants.WeightCoverterParams.VOLUME_CONVERTER,"Yes");
            stringRequest.putParams(Constants.WeightCoverterParams.LENGTH,request.getLength());
            stringRequest.putParams(Constants.WeightCoverterParams.WIDTH,request.getWidth());
            stringRequest.putParams(Constants.WeightCoverterParams.HEIGHT,request.getLength());
        }


        callback.setClazz(WeightConverterResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler getPlans(final StringRequestCallback<MyPlansResponse> callback, String user_id) {
        String getProfileUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.MEMBER_PLANS)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, getProfileUrl, callback);
        stringRequest.putParams(Constants.ProfileParams.USER_ID,user_id);
        callback.setClazz(MyPlansResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler changePassword(final StringRequestCallback<ForgotPasswordResponse> callback, String user_id,String old_password,String new_password) {
        String forgotPasswordUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.CHANGE_PASSWORD)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, forgotPasswordUrl, callback);
        stringRequest.putParams(Constants.ChangePasswordParams.USER_ID,user_id);
        stringRequest.putParams(Constants.ChangePasswordParams.NEW_PASSWORD,new_password);
        stringRequest.putParams(Constants.ChangePasswordParams.OLD_PASSWORD,old_password);
        callback.setClazz(ForgotPasswordResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }


    public RequestHandler shippingRates(final StringRequestCallback<ShippingRatesResponse> callback, String country, String carrier) {
        String shipping_ratesdUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.SHIPPING_RATES)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, shipping_ratesdUrl, callback);
        stringRequest.putParams(Constants.ShippingRatesParams.COUNTRY,country);
        stringRequest.putParams(Constants.ShippingRatesParams.CARRIER,carrier);
        callback.setClazz(ShippingRatesResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler getTestimonial(final StringRequestCallback<TestimonialResponse> callback) {
        String testimonialUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.TESTIMONIAL)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.GET, testimonialUrl, callback);
        callback.setClazz(TestimonialResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler doEditProfile(final StringRequestCallback<EditProfileResponse> callback, EditProfileRequest request) {
        String editProfileUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.EDIT_PROFILE)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, editProfileUrl, callback);
        callback.setClazz(EditProfileResponse.class);
        stringRequest.putParams(Constants.EditProfileParams.FIRST_NAME, request.getFirstname());
        stringRequest.putParams(Constants.EditProfileParams.LAST_NAME, request.getLastname());
        stringRequest.putParams(Constants.EditProfileParams.USER_ID, request.getUserid());
        stringRequest.putParams(Constants.EditProfileParams.MIDDLE_NAME, request.getMiddlename());
        stringRequest.putParams(Constants.EditProfileParams.DOB, request.getDob());
        stringRequest.putParams(Constants.EditProfileParams.GENDER, request.getGender());
        stringRequest.putParams(Constants.EditProfileParams.EMAIL, request.getEmail());
        stringRequest.putParams(Constants.EditProfileParams.ADDRESS1, request.getAddress1());
        stringRequest.putParams(Constants.EditProfileParams.ADDRESS2, request.getAddress2());
        stringRequest.putParams(Constants.EditProfileParams.CITY, request.getCity());
        stringRequest.putParams(Constants.EditProfileParams.POSTAL_CODE, request.getPincode());
        stringRequest.putParams(Constants.EditProfileParams.STATE, request.getState());
        stringRequest.putParams(Constants.EditProfileParams.COUNTRY, request.getCountry());
        stringRequest.putParams(Constants.EditProfileParams.TAXID, request.getTaxid());
        stringRequest.putParams(Constants.EditProfileParams.COUNTRY_CODE1, request.getCountrycode1());
        stringRequest.putParams(Constants.EditProfileParams.LOCAL_CODE1, request.getLocalcode1());
        stringRequest.putParams(Constants.EditProfileParams.PHONE1, request.getPhno1());
        stringRequest.putParams(Constants.EditProfileParams.COUNTRY_CODE2, request.getCountrycode2());
        stringRequest.putParams(Constants.EditProfileParams.LOCAL_CODE2, request.getLocalcode2());
        stringRequest.putParams(Constants.EditProfileParams.PHONE2, request.getPhno2());
        stringRequest.putParams(Constants.EditProfileParams.COUNTRY_CODE3, request.getCountrycode3());
        stringRequest.putParams(Constants.EditProfileParams.MOBILE_NUMBER, request.getMobile_no());
        stringRequest.putParams(Constants.EditProfileParams.FAX_NUMBER, request.getFaxno());
        stringRequest.putParams(Constants.EditProfileParams.USER_NAME, request.getUsername());
        stringRequest.putParams(Constants.EditProfileParams.COUNTRY, request.getCountry());
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler uploadProfileImage(final RequestCallback<EditProfileResponse> listener, FilePart image, String userid) {
        String editProfileUrl = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.UPLOAD_PROFILE_PIC)
                .build();

        MultiRequest<String, EditProfileResponse> gsonRequest = new MultiRequest<>(Request.Method.POST
                , editProfileUrl, listener, EditProfileResponse.class);
        gsonRequest.setShouldCache(false);
        gsonRequest.putFilePart("ProfileImage", image);
        gsonRequest.putStringPart("Userid", userid);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                11000,
                5,
                5f));
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler getShippingAddress(final StringRequestCallback<ShippingAddressResponse> callback, String userid) {
        String shipping_addressURL = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.SHIPPING_ADDRESS_LIST)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, shipping_addressURL, callback);
        stringRequest.putParams(Constants.ProfileParams.USER_ID,userid);
       
        callback.setClazz(ShippingAddressResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler addShippingAddress(final StringRequestCallback<ShippingAddressResponse> callback, ShippingAddressChangeRequest request) {
        String shipping_addressURL = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.ADD_SHIPPING_ADDRESS)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, shipping_addressURL, callback);
        stringRequest.putParams(Constants.AddhippingAddressParams.USER_ID,request.getUserid());
        stringRequest.putParams(Constants.AddhippingAddressParams.RECEIPT_NAME,request.getReceiptName());
        stringRequest.putParams(Constants.AddhippingAddressParams.ADDRESS,request.getAddress());
        stringRequest.putParams(Constants.AddhippingAddressParams.CITY,request.getCity());
        stringRequest.putParams(Constants.AddhippingAddressParams.STATE,request.getState());
        stringRequest.putParams(Constants.AddhippingAddressParams.COUNTRY,request.getCountry());
        stringRequest.putParams(Constants.AddhippingAddressParams.POSTAL_CODE,request.getPinCode());


        callback.setClazz(ShippingAddressResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }

    public RequestHandler deleteShippingAddress(final StringRequestCallback<ShippingAddressResponse> callback, String userid,String addressid) {
        String shipping_addressURL = new UrlBuilder()
                .appendPath(ApiConfig.ApiBaseMethods.DELETE_SHIPPING_ADDRESS)
                .build();
        GsonStringRequest stringRequest = new GsonStringRequest(Request.Method.POST, shipping_addressURL, callback);
        stringRequest.putParams(Constants.deleteShippingAddress.USER_ID,userid);
        stringRequest.putParams(Constants.deleteShippingAddress.ADDRESS_ID,addressid);

        callback.setClazz(ShippingAddressResponse.class);
        stringRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(stringRequest));
    }
}
