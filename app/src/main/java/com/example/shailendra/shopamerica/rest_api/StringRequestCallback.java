package com.example.shailendra.shopamerica.rest_api;

import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.shailendra.shopamerica.common.ApiConfig;
import com.example.shailendra.shopamerica.common.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
/**
 * Created by Administrator on 15-Apr-16.
 * al-oula-radio-app-android
 */
public abstract class StringRequestCallback<T> implements Response.Listener<String>, Response.ErrorListener {
    private Gson mGson = new Gson();
    private Class<T> clazz;

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void onSuccessResponse(T responseData) {
        onRestResponse(null, responseData);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (error.networkResponse == null) {
            error = new VolleyError(Constants.NO_RESPONSE);
            onRestResponse(error, null);
        } else if (error.networkResponse != null && error.networkResponse.data != null) {
            switch (error.networkResponse.statusCode) {
                case ApiConfig.ErrorCodes.SERVER_ERROR: {
                    error = new VolleyError(Constants.MESSAGE_SERVER_ERROR);
                    break;
                }
            }
            onRestResponse(error, null);
        } else if (error instanceof TimeoutError) {
            error = new VolleyError(Constants.REQUEST_TIME_OUT);
            onRestResponse(error, null);
        } else {
            onRestResponse(error, null);
        }
    }

    @Override
    public void onResponse(String response) {
        try {
            T object;
            if (clazz != String.class) {
                object = mGson.fromJson(response, clazz);
            } else {
                object = (T) response;
            }
            onSuccessResponse(object);
        } catch (JsonSyntaxException e) {
            onErrorResponse(new VolleyError("Some error occurred."));
        } catch (Exception e) {
            onErrorResponse(new VolleyError("Some error occurred."));
        }
    }

    public abstract void onRestResponse(Exception e, T result);
}
