package com.example.shailendra.shopamerica.rest_api;

import android.location.Location;
import android.text.TextUtils;
import android.util.Pair;

import com.example.shailendra.shopamerica.common.ApiConfig;

import java.util.ArrayList;
import java.util.List;

public class UrlBuilder {
    private final String mRootUrl;
    private final List<String> mPathsSegments;
    private final List<Pair<String, String>> mUrlParams;

    /**
     * Create a new instance.
     * Base Url is the one defined in {@link ApiConfig}
     */
    public UrlBuilder() {
        mRootUrl = ApiConfig.BASE_URL;
        mPathsSegments = new ArrayList<String>();
        mUrlParams = new ArrayList<Pair<String, String>>();
    }


    /**
     * Append a path segment to url e.g. {@code url/path/}.
     * Do not include leading or trailing "/" with path segment, these will be automatically inserted.
     *
     * @param path path
     * @return url builder.
     */
    public UrlBuilder appendPath(String path) {
        if (!TextUtils.isEmpty(path)) {
            mPathsSegments.add(path);
        }
        return this;
    }

    /**
     * Add an integer as a parameter.
     *
     * @param key   key
     * @param value value
     * @return url builder
     */
    public UrlBuilder addParamInteger(String key, Integer value) {
        addParamString(key, Integer.toString(value));
        return this;
    }

    /**
     * Add a long as a parameter.
     *
     * @param key   key
     * @param value value
     * @return url builder
     */
    public UrlBuilder addParamLong(String key, Long value) {
        addParamString(key, Long.toString(value));
        return this;
    }

    /**
     * Add a {@link Location} as a parameter.
     * Resulting text is of the format {@code "key=lat,long"}.
     *
     * @param key   key
     * @param value value
     * @return url builder
     */
    public UrlBuilder addParamLocation(String key, Location value) {
        if (value != null) {
            addParamString(key, value.getLatitude() + "," + value.getLongitude());
        }
        return this;
    }

    /**
     * Add a list as a parameters. Multiple values with same key will be added.
     *
     * @param key    key
     * @param values value list.
     * @return url builder
     */
    public UrlBuilder addParamLongList(String key, List<Long> values) {
        if (values != null) {
            for (long v : values) {
                addParamString(key, Long.toString(v));
            }
        }
        return this;
    }

    public UrlBuilder addExtraParams(List<Pair<String, String>> params) {
        if (params != null) {
            for (Pair<String, String> v : params) {
                addParamString(v.first, v.second);
            }
        }
        return this;
    }

    /**
     * Add a string key value parameter.
     *
     * @param key   key.
     * @param value value.
     * @return url builder.
     */
    public UrlBuilder addParamString(String key, String value) {

        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
            mUrlParams.add(new Pair<String, String>(key, value));
        }

        return this;
    }

    /**
     * Build the final url string.
     *
     * @return url string.
     */
    public String build() {
        StringBuilder sb = new StringBuilder(mRootUrl);

        //--Add paths--
        for (String pathSegment : mPathsSegments) {
            sb.append("/").append(pathSegment);
        }

        //--Add params--
        int count = 0;
        for (Pair<String, String> keyValue : mUrlParams) {
            sb.append(count > 0 ? "&" : "?")
                    .append(keyValue.first)
                    .append("=")
                    .append(keyValue.second);
            count++;
        }

        return sb.toString();
    }

}
