package com.example.shailendra.shopamerica.rest_api.model;

/**
 * Created by daffodil on 19/2/16.
 */
public class  ErrorModel {

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String status;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String Message;


    String response;
}
