package com.example.shailendra.shopamerica.rest_api.model.edit_profile;

/**
 * Created by daffodil on 13/7/16.
 */
public class EditProfileRequest {

    String Userid;
    String Firstname;
    String Middlename;
    String Lastname;
    String Dob;
    String Gender;
    String Email;
    String Address1;
    String Address2;
    String City;
    String Pincode;
    String State;
    String Country;
    String Taxid;
    String Countrycode1;
    String Localcode1;
    String Phno1;
    String Countrycode2;
    String Localcode2;
    String Phno2;
    String Countrycode3;
    String Mobile_no;
    String  Faxno;
    String  Username;

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String userid) {
        Userid = userid;
    }


    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getFaxno() {
        return Faxno;
    }

    public void setFaxno(String faxno) {
        Faxno = faxno;
    }

    public String getMobile_no() {
        return Mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        Mobile_no = mobile_no;
    }

    public String getCountrycode3() {
        return Countrycode3;
    }

    public void setCountrycode3(String countrycode3) {
        Countrycode3 = countrycode3;
    }

    public String getPhno2() {
        return Phno2;
    }

    public void setPhno2(String phno2) {
        Phno2 = phno2;
    }

    public String getLocalcode2() {
        return Localcode2;
    }

    public void setLocalcode2(String localcode2) {
        Localcode2 = localcode2;
    }

    public String getCountrycode2() {
        return Countrycode2;
    }

    public void setCountrycode2(String countrycode2) {
        Countrycode2 = countrycode2;
    }

    public String getPhno1() {
        return Phno1;
    }

    public void setPhno1(String phno1) {
        Phno1 = phno1;
    }

    public String getLocalcode1() {
        return Localcode1;
    }

    public void setLocalcode1(String localcode1) {
        Localcode1 = localcode1;
    }

    public String getCountrycode1() {
        return Countrycode1;
    }

    public void setCountrycode1(String countrycode1) {
        Countrycode1 = countrycode1;
    }

    public String getTaxid() {
        return Taxid;
    }

    public void setTaxid(String taxid) {
        Taxid = taxid;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getMiddlename() {
        return Middlename;
    }

    public void setMiddlename(String middlename) {
        Middlename = middlename;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    String  Company;
}
