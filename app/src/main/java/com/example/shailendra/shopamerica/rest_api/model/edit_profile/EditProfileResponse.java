package com.example.shailendra.shopamerica.rest_api.model.edit_profile;

/**
 * Created by daffodil on 13/7/16.
 */
public class EditProfileResponse {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    String status;
    String Message;
}
