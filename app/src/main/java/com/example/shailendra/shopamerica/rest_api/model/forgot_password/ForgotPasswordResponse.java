package com.example.shailendra.shopamerica.rest_api.model.forgot_password;

/**
 * Created by daffodil on 10/7/16.
 */
public class ForgotPasswordResponse {

    private String Message;

    private String status;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", status = "+status+"]";
    }
}
