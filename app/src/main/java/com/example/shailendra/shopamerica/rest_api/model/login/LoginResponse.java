package com.example.shailendra.shopamerica.rest_api.model.login;

/**
 * Created by daffodil on 3/6/16.
 */
public class LoginResponse {

    private Result result;

    private String Message;

    private String status;

    public String getSubcriptionStatus() {
        return SubcriptionStatus;
    }

    public void setSubcriptionStatus(String subcriptionStatus) {
        SubcriptionStatus = subcriptionStatus;
    }

    private String SubcriptionStatus;

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", Message = "+Message+", status = "+status+"]";
    }
}
