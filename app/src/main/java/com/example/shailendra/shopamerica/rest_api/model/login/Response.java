package com.example.shailendra.shopamerica.rest_api.model.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 3/6/16.
 */
public class Response {
    @SerializedName("response")
    private LoginResponse mResponse;

    public LoginResponse getResponse() {
        return mResponse;
    }

    public void setResponse(LoginResponse response) {
        mResponse = response;
    }
}
