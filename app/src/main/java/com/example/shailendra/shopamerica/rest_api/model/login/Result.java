package com.example.shailendra.shopamerica.rest_api.model.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 3/6/16.
 */
public class Result {

    private String Userid;

    @SerializedName("First Name")
    private String First_name;

    private String Username;

    public String getUserid ()
    {
        return Userid;
    }

    public void setUserid (String Userid)
    {
        this.Userid = Userid;
    }

    public String getFirst_name ()
    {
        return First_name;
    }

    public void setFirst_name (String First_name)
    {
        this.First_name = First_name;
    }

    public String getUsername ()
    {
        return Username;
    }

    public void setUsername (String Username)
    {
        this.Username = Username;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Userid = "+Userid+", First name = "+First_name+", Username = "+Username+"]";
    }
}
