package com.example.shailendra.shopamerica.rest_api.model.my_plans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 6/7/16.
 */
public class Business {
    @SerializedName("One Time Set-up Fee")
    private String One_Time_Set_upFee;

    @SerializedName("Monthly Fee")
    private String Monthly_Fee;

    @SerializedName("Personal US Address")
    private String Personal_US_Address;

    @SerializedName("Consolidation Services")
    private String Consolidation_Services;

    @SerializedName("Shipping Services to 223 countries")
    private String Shipping_Services_to_223_countries;

    @SerializedName("Repacking Services")
    private String Repacking_Services;

    @SerializedName("Delivery Destinations")
    private String Delivery_Destinations;

    @SerializedName("Personal Shopping Service Fee")
    private String Personal_Shopping_Service_Fee;

    @SerializedName("Imaging Services")
    private String Imaging_Services;

    @SerializedName("Inbound Packages")
    private String Inbound_Packages;

    @SerializedName("Dedicated Email Helpdesk")
    private String Dedicated_Email_Helpdesk;

    @SerializedName("Storage")
    private String Storage;

    @SerializedName("Shipping Discounts")
    private String Shipping_Discounts;

    @SerializedName("Current Plan")
    private String Current_Plan;

    public String getCurrent_Plan() {
        return Current_Plan;
    }

    public void setCurrent_Plan(String current_Plan) {
        Current_Plan = current_Plan;
    }

    public String getShipping_Discounts() {
        return Shipping_Discounts;
    }

    public void setShipping_Discounts(String shipping_Discounts) {
        Shipping_Discounts = shipping_Discounts;
    }

    public String getStorage() {
        return Storage;
    }

    public void setStorage(String storage) {
        Storage = storage;
    }

    public String getDedicated_Email_Helpdesk() {
        return Dedicated_Email_Helpdesk;
    }

    public void setDedicated_Email_Helpdesk(String dedicated_Email_Helpdesk) {
        Dedicated_Email_Helpdesk = dedicated_Email_Helpdesk;
    }

    public String getInbound_Packages() {
        return Inbound_Packages;
    }

    public void setInbound_Packages(String inbound_Packages) {
        Inbound_Packages = inbound_Packages;
    }

    public String getImaging_Services() {
        return Imaging_Services;
    }

    public void setImaging_Services(String imaging_Services) {
        Imaging_Services = imaging_Services;
    }

    public String getPersonal_Shopping_Service_Fee() {
        return Personal_Shopping_Service_Fee;
    }

    public void setPersonal_Shopping_Service_Fee(String personal_Shopping_Service_Fee) {
        Personal_Shopping_Service_Fee = personal_Shopping_Service_Fee;
    }

    public String getDelivery_Destinations() {
        return Delivery_Destinations;
    }

    public void setDelivery_Destinations(String delivery_Destinations) {
        Delivery_Destinations = delivery_Destinations;
    }

    public String getRepacking_Services() {
        return Repacking_Services;
    }

    public void setRepacking_Services(String repacking_Services) {
        Repacking_Services = repacking_Services;
    }

    public String getShipping_Services_to_223_countries() {
        return Shipping_Services_to_223_countries;
    }

    public void setShipping_Services_to_223_countries(String shipping_Services_to_223_countries) {
        Shipping_Services_to_223_countries = shipping_Services_to_223_countries;
    }

    public String getConsolidation_Services() {
        return Consolidation_Services;
    }

    public void setConsolidation_Services(String consolidation_Services) {
        Consolidation_Services = consolidation_Services;
    }

    public String getPersonal_US_Address() {
        return Personal_US_Address;
    }

    public void setPersonal_US_Address(String personal_US_Address) {
        Personal_US_Address = personal_US_Address;
    }

    public String getMonthly_Fee() {
        return Monthly_Fee;
    }

    public void setMonthly_Fee(String monthly_Fee) {
        Monthly_Fee = monthly_Fee;
    }

    public String getOne_Time_Set_upFee() {
        return One_Time_Set_upFee;
    }

    public void setOne_Time_Set_upFee(String one_Time_Set_upFee) {
        One_Time_Set_upFee = one_Time_Set_upFee;
    }

}
