package com.example.shailendra.shopamerica.rest_api.model.my_plans;

/**
 * Created by daffodil on 6/7/16.
 */
public class Result
{
    private Basic Basic;

    private Business Business;

    private Premium Premium;

    public Basic getBasic ()
    {
        return Basic;
    }

    public void setBasic (Basic Basic)
    {
        this.Basic = Basic;
    }

    public Business getBusiness ()
    {
        return Business;
    }

    public void setBusiness (Business Business)
    {
        this.Business = Business;
    }

    public Premium getPremium ()
    {
        return Premium;
    }

    public void setPremium (Premium Premium)
    {
        this.Premium = Premium;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Basic = "+Basic+", Business = "+Business+", Premium = "+Premium+"]";
    }
}
