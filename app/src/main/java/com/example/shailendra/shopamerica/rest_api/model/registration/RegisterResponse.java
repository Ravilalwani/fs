package com.example.shailendra.shopamerica.rest_api.model.registration;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 7/6/16.
 */
public class RegisterResponse {

    private String Currencycode;

    private String Userid;

    private String Amount;

    @SerializedName("First name")
    private  String First_name;

    private String Message;

    private String status;

    private String UserStatus;

    private String SellerId;

    private String SubcriptionStatus;

    private String Plan;

    public String getCurrencycode ()
    {
        return Currencycode;
    }

    public void setCurrencycode (String Currencycode)
    {
        this.Currencycode = Currencycode;
    }

    public String getUserid ()
    {
        return Userid;
    }

    public void setUserid (String Userid)
    {
        this.Userid = Userid;
    }

    public String getAmount ()
    {
        return Amount;
    }

    public void setAmount (String Amount)
    {
        this.Amount = Amount;
    }



    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getUserStatus ()
    {
        return UserStatus;
    }

    public void setUserStatus (String UserStatus)
    {
        this.UserStatus = UserStatus;
    }

    public String getSellerId ()
    {
        return SellerId;
    }

    public void setSellerId (String SellerId)
    {
        this.SellerId = SellerId;
    }

    public String getSubcriptionStatus ()
    {
        return SubcriptionStatus;
    }

    public void setSubcriptionStatus (String SubcriptionStatus)
    {
        this.SubcriptionStatus = SubcriptionStatus;
    }

    public String getPlan ()
    {
        return Plan;
    }

    public void setPlan (String Plan)
    {
        this.Plan = Plan;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Currencycode = "+Currencycode+", Userid = "+Userid+", Amount = "+Amount+",  Message = "+Message+", status = "+status+", UserStatus = "+UserStatus+", SellerId = "+SellerId+", SubcriptionStatus = "+SubcriptionStatus+", Plan = "+Plan+"]";
    }
}
