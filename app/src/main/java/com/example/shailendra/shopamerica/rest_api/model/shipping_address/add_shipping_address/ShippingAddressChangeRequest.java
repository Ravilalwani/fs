package com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address;

/**
 * Created by daffodil on 15/7/16.
 */
public class ShippingAddressChangeRequest {

    String Userid;
    String ReceiptName;
    String Address;
    String City;
    String PinCode;
    String State;
    String Country;

    public String getDefault() {
        return Default;
    }

    public void setDefault(String aDefault) {
        Default = aDefault;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPinCode() {
        return PinCode;
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getReceiptName() {
        return ReceiptName;
    }

    public void setReceiptName(String receiptName) {
        ReceiptName = receiptName;
    }

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String userid) {
        Userid = userid;
    }

    String Default;
}
