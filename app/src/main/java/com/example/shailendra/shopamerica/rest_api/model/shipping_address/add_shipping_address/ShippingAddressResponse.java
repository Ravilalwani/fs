package com.example.shailendra.shopamerica.rest_api.model.shipping_address.add_shipping_address;

/**
 * Created by daffodil on 15/7/16.
 */
public class ShippingAddressResponse {

    String status;
    String Message;

    public Result[] getResult() {
        return result;
    }

    public void setResult(Result[] result) {
        this.result = result;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    Result[] result;
}
