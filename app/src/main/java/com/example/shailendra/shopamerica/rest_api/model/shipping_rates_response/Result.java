package com.example.shailendra.shopamerica.rest_api.model.shipping_rates_response;

/**
 * Created by daffodil on 11/7/16.
 */
public class Result {

    private String Basic;

    private String Business;

    private String Weight;

    private String Premium;

    public String getBasic ()
    {
        return Basic;
    }

    public void setBasic (String Basic)
    {
        this.Basic = Basic;
    }

    public String getBusiness ()
    {
        return Business;
    }

    public void setBusiness (String Business)
    {
        this.Business = Business;
    }

    public String getWeight ()
    {
        return Weight;
    }

    public void setWeight (String Weight)
    {
        this.Weight = Weight;
    }

    public String getPremium ()
    {
        return Premium;
    }

    public void setPremium (String Premium)
    {
        this.Premium = Premium;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Basic = "+Basic+", Business = "+Business+", Weight = "+Weight+", Premium = "+Premium+"]";
    }
}
