package com.example.shailendra.shopamerica.rest_api.model.testimonial;

/**
 * Created by daffodil on 11/7/16.
 */
public class Result {

    private String Name;

    private String Message;

    private String Country;

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }



    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }




}
