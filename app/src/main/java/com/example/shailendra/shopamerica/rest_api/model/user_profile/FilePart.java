package com.example.shailendra.shopamerica.rest_api.model.user_profile;

import java.io.File;

/**
 * Created by vipulsharma on 27/10/15.
 */
public class FilePart {

    private File mFile;
    private String mMimeType;

    public FilePart(File file, String mimeType) {
        mFile = file;
        mMimeType = mimeType;
    }

    public File getFile() {
        return mFile;
    }

    public FilePart setFile(File profileImage) {
        mFile = profileImage;
        return this;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public FilePart setMimeType(String mimeType) {
        mMimeType = mimeType;
        return this;
    }
}
