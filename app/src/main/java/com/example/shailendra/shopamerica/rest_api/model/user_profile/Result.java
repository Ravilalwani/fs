package com.example.shailendra.shopamerica.rest_api.model.user_profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 10/7/16.
 */
public class Result {

    private String UserType;

    private String Userid;

    private String Phone1;

    private String Phone2;

    private String FaxNo;

    private String Dob;

    private String TaxID;

    private String MiddleName;

    private String ProfileImg;

    private String Email;

    private String Gender;

    private String Username;

    private String Address;

    private String Company;

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    @SerializedName("Last Name")
    private String Last_Name;

    @SerializedName("First Name")
    private String First_Name;

    public String getUserType ()
    {
        return UserType;
    }

    public void setUserType (String UserType)
    {
        this.UserType = UserType;
    }

    public String getUserid ()
    {
        return Userid;
    }

    public void setUserid (String Userid)
    {
        this.Userid = Userid;
    }

    public String getPhone1 ()
    {
        return Phone1;
    }

    public void setPhone1 (String Phone1)
    {
        this.Phone1 = Phone1;
    }

    public String getPhone2 ()
    {
        return Phone2;
    }

    public void setPhone2 (String Phone2)
    {
        this.Phone2 = Phone2;
    }

    public String getFaxNo ()
    {
        return FaxNo;
    }

    public void setFaxNo (String FaxNo)
    {
        this.FaxNo = FaxNo;
    }

    public String getDob ()
    {
        return Dob;
    }

    public void setDob (String Dob)
    {
        this.Dob = Dob;
    }

    public String getTaxID ()
    {
        return TaxID;
    }

    public void setTaxID (String TaxID)
    {
        this.TaxID = TaxID;
    }

    public String getMiddleName ()
    {
        return MiddleName;
    }

    public void setMiddleName (String MiddleName)
    {
        this.MiddleName = MiddleName;
    }

    public String getProfileImg ()
    {
        return ProfileImg;
    }

    public void setProfileImg (String ProfileImg)
    {
        this.ProfileImg = ProfileImg;
    }

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getGender ()
    {
        return Gender;
    }

    public void setGender (String Gender)
    {
        this.Gender = Gender;
    }

    public String getUsername ()
    {
        return Username;
    }

    public void setUsername (String Username)
    {
        this.Username = Username;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getCompany ()
    {
        return Company;
    }

    public void setCompany (String Company)
    {
        this.Company = Company;
    }




}
