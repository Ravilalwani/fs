package com.example.shailendra.shopamerica.rest_api.model.weight_converter;

/**
 * Created by daffodil on 10/7/16.
 */
public class ConverterRequest {

    String Weightconverter;
    String Unit;
    String Convertvalue;
    String Volumeconverter;
    String Length;
    String Width;
    String Height;

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getVolumeconverter() {
        return Volumeconverter;
    }

    public void setVolumeconverter(String volumeconverter) {
        Volumeconverter = volumeconverter;
    }

    public String getConvertvalue() {
        return Convertvalue;
    }

    public void setConvertvalue(String convertvalue) {
        Convertvalue = convertvalue;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getWeightconverter() {
        return Weightconverter;
    }

    public void setWeightconverter(String weightconverter) {
        Weightconverter = weightconverter;
    }




}
