package com.example.shailendra.shopamerica.rest_api.model.weight_converter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 10/7/16.
 */
public class Result {

    private String Pound;

    private String Kilogram;

    public String getDimensional_Weight_Results() {
        return Dimensional_Weight_Results;
    }

    public void setDimensional_Weight_Results(String dimensional_Weight_Results) {
        Dimensional_Weight_Results = dimensional_Weight_Results;
    }

    @SerializedName("Dimensional Weight Results")
    private String Dimensional_Weight_Results;

    public String getPound ()
    {
        return Pound;
    }

    public void setPound (String Pound)
    {
        this.Pound = Pound;
    }

    public String getKilogram ()
    {
        return Kilogram;
    }

    public void setKilogram (String Kilogram)
    {
        this.Kilogram = Kilogram;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Pound = "+Pound+", Kilogram = "+Kilogram+"]";
    }
}
