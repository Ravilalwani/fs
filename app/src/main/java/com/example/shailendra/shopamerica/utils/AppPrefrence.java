package com.example.shailendra.shopamerica.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by daffodil on 8/7/16.
 */
public class AppPrefrence {

    private static final String USER_ID = "_user_id";
    private static final String USER_IMAGE = "_user_image";
    private static final String USER_NAME = "_user_name";
    private static String Foreign_Shopper_PREFRENCE = "_foreign_shopper";

    private static SharedPreferences sharedPreferences;
    private static AppPrefrence appPreference;

    public static AppPrefrence getInstance() {
        if (appPreference == null) {
            appPreference = new AppPrefrence();
        }
        return appPreference;
    }

    private SharedPreferences getSharedPreferences(Context _context) {
        if (sharedPreferences == null) {
            sharedPreferences = _context.getSharedPreferences(Foreign_Shopper_PREFRENCE, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public void clearPrefrences(Context context)
    {
        context.getSharedPreferences(Foreign_Shopper_PREFRENCE,0).edit().clear().commit();
    }

    public void saveUserId(Context _context, String userid) {
        SharedPreferences s = getSharedPreferences(_context);
        SharedPreferences.Editor e = s.edit();
        if (userid != null) {
            e.putString(USER_ID, userid);

        }
        e.commit();
    }

    public String getUserId(Context con)
    {
        SharedPreferences settings = con.getSharedPreferences(Foreign_Shopper_PREFRENCE, Context.MODE_PRIVATE);
        return settings.getString(USER_ID, null);
    }

    public void saveImageUrl(Context _context, String imgurl) {
        SharedPreferences s = getSharedPreferences(_context);
        SharedPreferences.Editor e = s.edit();
        if (imgurl != null) {
            e.putString(USER_IMAGE, imgurl);

        }
        e.commit();
    }

    public String getUSerImageUrl(Context con)
    {
        SharedPreferences settings = con.getSharedPreferences(Foreign_Shopper_PREFRENCE, Context.MODE_PRIVATE);
        return settings.getString(USER_IMAGE, null);
    }

    public void saveUserName(Context _context, String name) {
        SharedPreferences s = getSharedPreferences(_context);
        SharedPreferences.Editor e = s.edit();
        if (name != null) {
            e.putString(USER_NAME, name);

        }
        e.commit();
    }

    public String getUserName(Context con)
    {
        SharedPreferences settings = con.getSharedPreferences(Foreign_Shopper_PREFRENCE, Context.MODE_PRIVATE);
        return settings.getString(USER_NAME, null);
    }


}
