package io.futurestud.tutorials.customfont;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by norman on 3/8/15.
 */
public class CustomFontTextView extends TextView{

    public CustomFontTextView(Context context) {
        super(context);

        io.futurestud.tutorials.customfont.CustomFontUtils.applyCustomFont(this, context, null);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

       io.futurestud.tutorials.customfont.CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        io.futurestud.tutorials.customfont.CustomFontUtils.applyCustomFont(this, context, attrs);
    }
}